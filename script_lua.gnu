clear
reset
unset key

# Set output
set terminal jpeg size 1920,1080
set output 'lua.jpeg'

# Make the x axis labels easier to read.
set xtics rotate out

# Select histogram data
# set style data histogram
set style data linespoints

# Give the bars a plain fill pattern, and draw a solid line around them.
set style fill solid border

set style histogram clustered
# set style histogram rowstacked
# set boxwidth 0.8 #rowstacked only

set key noenhanced

set xlabel 'Loops'
set ylabel 'Amount'

plot for [COL=1:5] 'results_lua.txt' using COL:xtic(6) title columnheader

# plot \
# newhistogram "Instructions", 'program.dat' using 1:xticlabels(4) t col, \
# newhistogram "Reduction",  '' u 2:xticlabels(4) t col, '' u 3:xticlabels(4) t col
