#!/bin/bash

SPAN=32000

while [[ "$SPAN" -le 64000 ]];
do
    echo $SPAN
    START=35000000
    CNT=0
    while [[ "$CNT" -lt 8 ]];
    do
        echo $(($START + $CNT * $SPAN)) $(($START + $CNT * $SPAN + $SPAN))
        ./dr611/bin64/drrun -stack_size 1MB -c ./dr611/samples/bin/libreopt.so $(($START + $CNT * $SPAN)) $(($START + $CNT * $SPAN + $SPAN)) -- $@
        OUTPUT=`cat symbolic.out`
        echo "$OUTPUT -- $@ -- i$(($START + $CNT * $SPAN)) - i$(($START + $CNT * $SPAN + $SPAN))" >> results.txt
        CNT=$(($CNT + 1))
    done
    SPAN=$(($SPAN * 2))
done

