#!/bin/bash
LIB=$1
PROG=$2
shift 2
../dr611/bin64/drrun -stderr_mask 15 -stack_size 5M -c ../dr611/samples/bin/lib$LIB.so -- $PROG $@

