#!/bin/bash
ALIASFILE="./aliases.out"
PROG="$1"
shift

if [[ ! -f "$ALIASFILE" ]];
then
	printf "PROGRAM\t\t\tDISC.\t\tMAYBE\t\tNOT DISC.\tRAR\tWAW\tRAW\tWAR\n" > "$ALIASFILE"
fi
printf "%-15s\t" "$PROG" >> "$ALIASFILE"
./dr/bin64/drrun -dr_home ./dr/ -pause_on_error -c ./dr/samples/bin/libalias_eval.so -- ./testbed/bin/$PROG $@
