#!/bin/bash
LIBLOC="./dr611/samples/bin/libtest_reopt.so"

for i in `seq 1 60`; do
    ./dr611/bin64/drrun -stderr_mask 15 -stack_size 8MB -c $LIBLOC $i -- ~/ClonedProjects/lua/src/lua ~/ClonedProjects/lua/fact.lua
    echo "`cat symbolic.out` $i" >> results_lua.txt
done

