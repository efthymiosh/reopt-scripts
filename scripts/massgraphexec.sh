#!/bin/bash
LIB="test_reopt"
PROGS=`ls ./testbed/bin/ts_* | sed 's/.*\/\(.*\)/\1/'`

for PROG in $PROGS; do
    ./dr611/bin64/drrun -stderr_mask 15 -c ./dr611/samples/bin/lib$LIB.so -- ./testbed/bin/$PROG
    dot graph.dot -Tpng -o "$PROG".png
done
