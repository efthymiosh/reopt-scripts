#!/bin/bash
LIBLOC="./dr611/samples/bin/libtest_reopt.so"
PROGS=`ls ./testbed/bin/ts_*`
for PROG in $PROGS; do
    ./dr611/bin64/drrun -stderr_mask 15 -c $LIBLOC -- $PROG
    echo "`cat symbolic.out` -- $PROG" >> results.txt
done

