#!/bin/bash

cat results.txt | awk '{print $1 " " $2 " " $3 " " $9}' | sed 's/[^ ]*\/\(.*\)/\1/' | sed 's/\_/\\\\\_/g' > program.dat

gnuplot script.gnu
