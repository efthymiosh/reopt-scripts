#!/bin/bash
LIBLOC="./dr611/samples/bin/libtest_reopt.so"

for i in `seq 1 60`; do
    ./dr611/bin64/drrun -stderr_mask 15 -stack_size 8MB -c $LIBLOC $i -- ~/cloned/mini-interpreter/mini-int ~/cloned/mini-interpreter/examples/loop.mi
    echo "`cat symbolic.out` $i" >> results_mi.txt
done

