	.file	"branches.c"
	.section	.rodata
.LC0:
	.string	"ar[%lld] = %lld\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$4032, %rsp
	movq	$0, -24(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -32(%rbp)
	movq	$0, -8(%rbp)
	jmp	.L2
.L5:
	movq	-16(%rbp), %rax
	addq	$2, %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	addq	$499, %rax
	cmpq	$998, %rax
	jbe	.L3
	movq	-16(%rbp), %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, -24(%rbp)
	jmp	.L4
.L3:
	movq	-16(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, -24(%rbp)
.L4:
	movq	-32(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	imulq	-16(%rbp), %rsi
	movq	-16(%rbp), %rcx
	movabsq	$-9214356032908681207, %rdx
	movq	%rcx, %rax
	imulq	%rdx
	leaq	(%rdx,%rcx), %rax
	movq	%rax, %rdx
	sarq	$9, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	leaq	(%rsi,%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, -4032(%rbp,%rax,8)
	addq	$1, -8(%rbp)
.L2:
	cmpq	$499, -8(%rbp)
	jle	.L5
	movq	$0, -8(%rbp)
	jmp	.L6
.L7:
	movq	-8(%rbp), %rax
	movq	-4032(%rbp,%rax,8), %rdx
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	addq	$1, -8(%rbp)
.L6:
	cmpq	$499, -8(%rbp)
	jle	.L7
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
