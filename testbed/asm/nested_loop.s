	.file	"nested_loop.c"
	.section	.rodata
.LC0:
	.string	"%5d"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1040, %rsp
	movl	$0, -1040(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L2
.L5:
	movl	-4(%rbp), %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%eax, %edx
	movl	-4(%rbp), %eax
	cltq
	salq	$6, %rax
	addq	%rbp, %rax
	subq	$1040, %rax
	movl	%edx, (%rax)
	movl	$1, -8(%rbp)
	jmp	.L3
.L4:
	movl	-8(%rbp), %eax
	subl	$1, %eax
	cltq
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	-1040(%rbp,%rax,4), %eax
	leal	(%rax,%rax), %edx
	movl	-8(%rbp), %eax
	cltq
	movl	-4(%rbp), %ecx
	movslq	%ecx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movl	%edx, -1040(%rbp,%rax,4)
	addl	$1, -8(%rbp)
.L3:
	cmpl	$15, -8(%rbp)
	jle	.L4
	addl	$1, -4(%rbp)
.L2:
	cmpl	$15, -4(%rbp)
	jle	.L5
	movl	$0, -4(%rbp)
	jmp	.L6
.L9:
	movl	$0, -8(%rbp)
	jmp	.L7
.L8:
	movl	-8(%rbp), %eax
	cltq
	movl	-4(%rbp), %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	addq	%rdx, %rax
	movl	-1040(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -8(%rbp)
.L7:
	cmpl	$15, -8(%rbp)
	jle	.L8
	movl	$10, %edi
	call	putchar
	addl	$1, -4(%rbp)
.L6:
	cmpl	$15, -4(%rbp)
	jle	.L9
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
