#include "distribution.h"
////////////////////////Random NumberGenerator Class////////////////////////////
Distribution::Distribution(){
  srand(time(NULL));
}

/* Synartisi paragwgis tyxaiwn arithmwn omoiomorfis katanomis:
 * Thewroume oti i gennitria tyxaiwn tis ANSI C parexei kathara tyxaious arithmous.*/
int Distribution::uniform(int lo, int hi){
  return rand() % (hi - lo + 1) + lo;
}

/* Synartisi paragwgis tyxaiwn arithmwn ekthetikis katanomis: */
int Distribution::exponential(int mean) {
  double temp;
  temp = (double) rand() / RAND_MAX;
  temp = -(log(1 - temp))*mean;
  return (int) temp;
}