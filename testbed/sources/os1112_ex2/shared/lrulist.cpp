#include "classes.h"

LRUList::LRUList(int mem_size, int page_size){
  first = last = NULL;
  ram_pages = mem_size / page_size;
  this->page_size = page_size;
  amt = 0;
  page_faults = 0;
  page_requests = 0;
  disk_buf = 0;
  ram_buf = 0;
  dumpe = 0;
  return;
}

LRUList::~LRUList(void){
  Page *temp = first;
  if (temp) {
    while(temp = temp->get_next())
      delete temp->get_prev();
    delete temp;
  }
  return;
}  

int LRUList::getAmt(void){
  return amt;
}

int LRUList::setAmt(int newamt){
  int temp = amt;
  amt = newamt;
  return newamt - amt;
}

int LRUList::insert(pentry* data){
  Page *temp = last;
  while(amt >= ram_pages) {//Moving Least Recently Used page to secondary memory.
    if ((temp->get_data())->ishere()){
      (temp->get_data())->set_here(false);
      if (!(temp->get_data())->isclean()){
        disk_buf += page_size;
        ram_buf += page_size;
      }
      --amt;
    }
    temp = temp->get_prev();
  }
  if (first == NULL){
    ram_buf += page_size;
    if((first = last = new Page(NULL, data, NULL)) == NULL){
      cerr << "LRUList::insert(): Unable to allocate memory.." << endl;
      return 1;
    }
    ++amt;
    return 0;
  }
  temp = first;
  ram_buf += page_size;
  if((first = new Page(NULL, data, temp)) == NULL){
    cerr << "LRUList::insert(): Unable to allocate memory.." << endl;
    return 1;
  }
  ++amt;
  temp->set_prev(first);
  return 0;
}


pentry* LRUList::fetch(int page_id, int pid) {
  Page *temp, *retval = first;
  page_requests++;
  retval = first;
  while (retval) {
    if (((retval->get_data())->get_id() == page_id) && ((retval->get_data())->owner() == pid))
      break;
    retval = retval->get_next();
  }
  if (retval){ //an vrethike
    if (!(retval->get_data())->ishere()){
      ++page_faults;
      if (amt >= ram_pages){
        temp = last;
        while(temp) {//Moving Least Recently Used page to secondary memory.
          if ((temp->get_data())->ishere()){
            (temp->get_data())->set_here(false);
            if (!(temp->get_data())->isclean()){
              ram_buf += page_size;
              disk_buf += page_size;
              (temp->get_data())->make_clean();
            }
            if (amt <= ram_pages)
              break;            
            amt--;
            //to break ginetai prin, gia na min 3anaau3anetai axreiasta to amt meta to while.
          }
        temp = temp->get_prev();
        }
      }
      ram_buf += page_size;
      disk_buf += page_size;
      (retval->get_data())->set_here(true); //ean to amt einai mikrotero, tote apla ginetai auto.
    }
    return retval->get_data();
  }
  return NULL;
}

void LRUList::update(pentry *data){
  Page *temp = first, *conp, *conn;
  while (temp) {
    if ((temp->get_data()) == data){
      if (temp == first)
        return;
      if (temp == last)
        last = last->get_prev(); //not first, alliws tha epestrefe apo prin.
      conn = temp->get_next();
      conp = temp->get_prev();
      if (conp != NULL)
        conp->set_next(conn);
      if (conn != NULL)
        conn->set_prev(conp);
      else //tote to temp einai o last.
        last = conp; //kai metavalloume ton last ston proigoumeno.
      //to temp prosartietai stin arxi.
      first->set_prev(temp);
      temp->set_next(first);
      temp->set_prev(NULL);
      first = temp;
      return;
    }
    temp = temp->get_next();
  }
  //NOT REACHED
  cerr << "Error in code." << endl;
  return;
}

void LRUList::parse_list(void){
  Page *temp = first;
  for(int i = 1; temp != NULL; i++){
    cout << "Elem(" << i <<"): ";
    if (!temp->get_data())
      cout << "ERROR" << endl;
    else
      cout << "Owner: " << temp->get_data()->owner() << ", ID:" << temp->get_data()->get_id() << endl;
    temp = temp->get_next();
  }
}

void LRUList::rem_procmem(int pid){
  Page *temp = first, *conp, *conn;
  while (temp){
    if ((temp->get_data())->owner() == pid){
      if ((temp->get_data())->ishere())
        --amt;
      conn = temp->get_next();
      conp = temp->get_prev();
      if (conp != NULL)
        conp->set_next(conn);
      if (conn != NULL)
        conn->set_prev(conp);
      else //temp == last
        last = temp->get_prev();
      if (temp == first)
        first = conn;
      delete temp;
      temp = conn;
      continue;
    }
    else 
      temp = temp->get_next();
  }
  return;
}

int LRUList::get_pfaults(void){
  return page_faults;
}

int LRUList::get_prequests(void){
  return page_requests;
}

void LRUList::flush(long& ram, long& disk){
  ram = ram_buf;
  disk = disk_buf;
  ram_buf = disk_buf = 0;
  return;
}

long LRUList::dumped (void){
  return dumpe;
}

long LRUList::restore (void){
  long temp = dumpe;
  dumpe = 0;
  return temp;
}

void LRUList::dump (void){
  dumpe = ram_pages * page_size;
  return;
}

void LRUList::reset_pfaults(void){
  page_faults = 0;
  return;
}

int LRUList::active_pages(void){
  return ram_pages;
}

void LRUList::increase_pages(int i){
  ram_pages += i;
  return;
}

bool LRUList::decrease_pages(int i){
  if (ram_pages - i > 0){
    ram_pages -= i;
    return true;
  }
  return false;
}