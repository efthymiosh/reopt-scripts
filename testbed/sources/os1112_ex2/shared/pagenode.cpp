#include "classes.h"

Page::Page(Page *prev, pentry* data, Page *next){
  this->prev = prev;
  this->next = next;
  next = NULL;
  this->data = data;
  return;
}

Page::~Page(void){
  if (data)
    delete data;
  return;
}

void Page::set_data(pentry* data){
  this->data = data;
  return;
}

pentry* Page::get_data(void){
  return data;
}

void Page::set_next(Page* next){
  this->next = next;
  return;
}

Page* Page::get_next(void){
  return next;
}

void Page::set_prev(Page* prev){
  this->prev = prev;
  return;
}

Page* Page::get_prev(void){
  return prev;
}

int Page::compare(Page *other){
  return 0; //DELETE THIS FUNCTION //data->get_age() - (other->data)->get_age(); // Megalytero: <0 allo , >0 auto. = 0 isa. 
}

