#include "classes.h"

pentry::pentry(int owner, int id){
  //age = 0ULL;
  is_here = true;
  clean = false; //Oles oi selides 3ekinoun "vrwmikes" dioti den exoun eggrafei sto disko.
  pr_id = owner;
  this->id = id;
  return;
}

pentry::pentry(bool h, bool c, int owner, int id){
  //age = 0ULL;
  is_here = h;
  clean = c;
  pr_id = owner;
  this->id = id;
  return;
}

void pentry::set_here(bool set) {
  is_here = set;
  return;
}

bool pentry::ishere(void) {
  return is_here;
}

/*void pentry::set_clean(bool set) {
  clean = set;
  return;
}

bool pentry::isclean(void) {
  return clean;
}

unsigned long long pentry::get_age(void) {
  return age;
}*/

/*void pentry::get older(bool accessed) {
  age >>= 1;
  if (accessed) {
    unsigned long long temp = 1 << (sizeof(unsigned long long) * 8); 
    age |= temp;
  }
  return;
}*/

int pentry::get_id(void) {
  return id;
}

int pentry::owner(void) {
  return pr_id;
}

void pentry::make_dirty(void){
  clean = false;
  return;
}

void pentry::make_clean(void){
  clean = true;
  return;
}

bool pentry::isclean(void){
  return clean;
}
