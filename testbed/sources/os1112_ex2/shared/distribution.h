#ifndef DISTRIH
#define DISTRIH

#include <climits>
#include <cstdlib>
#include <ctime>
#include <cmath>

/* I distribution dimiourgeitai me tin texniki singleton gia dieukolynsi
 * stin eggrafi kai katanoisi tou ypoloipou kwdika*/
class Distribution { //Random number generator class
private:
  Distribution();                                 // Private constructor
public:
  static Distribution& instance(){
    static Distribution singleton; //stin prwti klisi dimiourgeitai, meta epistrefetai.
    return singleton;
  }
  int uniform(int, int);
  int exponential(int); //int; not double, as needed by the simulator.
};

#endif
