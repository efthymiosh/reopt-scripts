#include "classes.h"
#include "distribution.h"

MemUnit::MemUnit(int lo, int hi, int p_size, int mem_size, int sim_dur, int t, int T){
  if ((plist = new LRUList(mem_size, p_size)) == NULL){
    cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
    return;
  }
  this->lo = lo;
  this->hi = hi;
  this->p_size = p_size;
  this->mem_size = mem_size;
  this->t = t;
  this->T = T;
  sts = sim_dur;
  if ((pr_array = new Process*[pr_size = (sim_dur / T * 4)]) == NULL){ //To megethos ypologistike empeirika. Ginetai kai elegxos.
      cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
      return;
    }
  pr_in = 0;
  return;
}

MemUnit::~MemUnit(void){
  delete plist;
  while(pr_in--)
    delete pr_array[pr_in];
  delete[] pr_array;
  return;
}

int MemUnit::operate(long& tpfaults, long& tpreqs, long& tproc, double& avg_turn, double& avg_req_ppf){
  int temp, i, frozen, defrost;
  pentry *fetched;
  long long time;
  long long spawn;
  long total_life = 0;
  long amt_proc = 0;
  long drsr; //disk to ram speed ratio
  long ram_buf, disk_buf, ram_temp, disk_temp, pr_life;
  pentry *tentry;
  Distribution gener = Distribution::instance();
  if (pr_array == NULL)
    return 1;
  drsr = 20; //Thewreitai RAM me 100MB/s RW, diskos me 5MB/s RW.

  spawn = time = sts * 100 * 1024; //STS * seconds * 100 * MB / seconds = STS * 100 * 1024 * KB. O xronos mas metrietai se KB.
  if (spawn < 0) //overflow
    return 1;
  while (time > 0){ //MAIN LOOP
    ram_buf = 0;
    if (time <= spawn) { //Time to generate new process
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      if (pr_in == pr_size) { //process array full.
        Process **temp = pr_array;
        pr_size *= 2;
        if ((pr_array = new Process*[pr_size]) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        for (i = 0; i < pr_in; i++)
          pr_array[i] = temp[i];
        delete[] temp;
      }
      amt_proc++; //could be replaced by id.
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      if((pr_array[pr_in] = new Process(temp = gener.uniform(lo, hi) / p_size + 1, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plist->insert(tentry);
      }
      plist->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    if (!pr_in){ //not a normal time to generate process, but no processes alive, so time is frozen.
      time = spawn;
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      amt_proc++;
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      if((pr_array[pr_in] = new Process(temp = gener.uniform(lo, hi) / p_size + 1, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plist->insert(tentry);
      }
      plist->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    //Proceeding with round-robin execution of the processes.
    frozen = 0; //Used to check if all processes are waiting for the disk.
    defrost = 0;
    for(i = 0; i < pr_in; i++){
      if (pr_array[i]->waiting()){
        frozen++;
        if (pr_array[defrost]->get_waittime() > pr_array[i]->get_waittime())
          defrost = i;
          //An den einai pagwmeni i 1i den leitourgei swsta. Alla auto de mas endiaferei.
        continue; //I process perimenei to disko.
      }
      if (pr_array[i]->waiting())
        continue; //I process perimenei to disko.
      
      temp = pr_array[i]->request(); //Page request.
      fetched = plist->fetch(temp, pr_array[i]->get_pid());
      plist->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[i]->set_waittime(disk_temp * drsr);
      if (pr_array[i]->operate(fetched)) //ean egine allagi sta dedomena
        ram_buf += gener.exponential((lo + hi) / 2); // ekthetika katanemimeno me mesi timi ti mesi mnimi pou katalamvanei mia diadikasia.
                                                     //Thewreitai diladi, oti i mesi diadikasia tha metavalei kata meso oro ligotera apo ta misa tis.
      ram_buf += gener.exponential((lo + hi) / 2);   //I idia diadikasia gia to diavasma dedomenwn.
      plist->update(fetched);
    }
    /* An oles oi processes perimenoun to disko apo ton proigoumeno kyklo, den yparxei kamia 
     * gia na kinisei to xrono, o opoios xronos metrietai se bytes tis RAM.
     * Gia na proxwrisw to xrono to metavallw apeutheias analoga me ti process pou
     * xreiazetai to mikrotero xrono gia na 3ekleidwthei.
     */
    if (frozen == pr_in){ //to pr_in einai != 0 edw, de xreiazetai na to eleg3oume.
      ram_buf = pr_array[defrost]->get_waittime(); //i ram_buf xrisimopoieitai san temp.
      for(i = 0; i < pr_in; i++) //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    else{
      for(i = 0; i < pr_in; i++)  //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    for(i = 0; i < pr_in; i++)  //reduce lifespan
      pr_array[i]->age(ram_buf);
    time -= ram_buf;
    //End of cycle, removing dying processes and their according pages.
    temp = 0; //Used to assign shift amount, in case of deletion.
    for(i = 0; i < pr_in; i++){ // if no processes, this is not executed.
      //cout << "pr_size: " << pr_size << "- temp: " << temp <<" > i: " << i << endl;
      if (pr_size - temp > i) {
        pr_array[i] = pr_array[i + temp];
        //cout << "pr_array[" << i << "] = pr_array[" <<i<<" + "<<temp<<"]" <<endl;
      }
      if (pr_array[i]->isdead()){
        plist->rem_procmem(pr_array[i]->get_pid());
        delete pr_array[i];
        temp++;
        --i;
        --pr_in;
      }
    }
  }
  //Present data.
  tpfaults = plist->get_pfaults();
  tpreqs = plist->get_prequests();
  tproc = amt_proc;
  avg_turn = (double)total_life / amt_proc;
  avg_req_ppf = (double)plist->get_prequests() / plist->get_pfaults();
  return EXIT_SUCCESS;
}
