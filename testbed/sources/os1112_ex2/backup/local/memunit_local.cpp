#include "classes.h"
#include "distribution.h"

MemUnit::MemUnit(int lo, int hi, int p_size, int mem_size, int sim_dur, int t, int T, int page_min, int page_max){
  this->lo = lo;
  this->hi = hi;
  this->p_size = p_size;
  this->mem_size = mem_size;
  this->t = t;
  this->T = T;
  min_pages = page_min;
  max_pages = page_max;
  sts = sim_dur;
  if ((pr_array = new Process*[pr_size = (sim_dur / T * 4)]) == NULL){ //To megethos ypologistike empeirika. Ginetai kai elegxos.
      cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
      return;
  }
  if ((plr_array = new LRUList*[pr_size]) == NULL){
    cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
    return;
  }
  pr_in = 0;
  return;
}

MemUnit::~MemUnit(void){
  while(pr_in--){
    delete pr_array[pr_in];
    delete plr_array[pr_in];
  }
  delete[] pr_array;
  delete[] plr_array;
  return;
}

int MemUnit::operate(long& tpfaults, long& tpreqs, long& tproc, double& avg_turn, double& avg_req_ppf){
  int temp, analog_assign, frozen, defrost, i, j, cur_pages;
  pentry *fetched;
  double analog_ratio;
  long long time;
  long long spawn;
  long total_life = 0;
  long amt_proc = 0;
  long drsr; //disk to ram speed ratio
  long ram_buf, disk_buf, ram_temp, disk_temp, pr_life;
  pentry *tentry;
  Distribution gener = Distribution::instance();
  tpfaults = 0;
  tpreqs = 0;
  if (pr_array == NULL){
    cerr << "MemUnit::operate(): No memory assigned to the process array" << endl;
    return 1;
  }
  if (plr_array == NULL){
    cerr << "MemUnit::operate(): No memory assigned to the LRU array" << endl;
    return 1;
  }
  if ((p_size * max_pages) > mem_size){
    cout << "MemUnit::operate(): Maximum process memory (" << p_size * max_pages << ") larger than total available RAM (" << mem_size << ")" << endl; //ean de mporei na xwresei tin pio megali diergasia
    return 1;
  }
  drsr = 20;
  cur_pages = 0;
  if (hi == lo)
    analog_ratio = 0;
  else if (max_pages >= min_pages)
    analog_ratio = (double) (max_pages - min_pages) / ((hi - lo) / p_size + 1);
  else{
    cout << "MemUnit::operate(): Maximum amount of pages is less than the minimum amount of pages. Logical error" << endl;
    return 1;
  }
  spawn = time = sts * 100 * 1024; //STS * seconds * 100 * MB / seconds = STS * 100 * 1024 * KB. O xronos metrietai se KB.
  if (spawn < 0){
    cout << "MemUnit::operate(): Counter Variable overflow" << endl;
    return 1;
  }
  while (time > 0){ //MAIN LOOP
    ram_buf = 0;
    if (time <= spawn) { //Time to generate new process
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      if (pr_in == pr_size) { //process array full.
        Process **temp = pr_array;
        LRUList **templ = plr_array;
        pr_size *= 2;
        if ((pr_array = new Process*[pr_size]) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        if ((plr_array = new LRUList*[pr_size]) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        for (i = 0; i < pr_in; i++){
          pr_array[i] = temp[i];
          plr_array[i] = templ[i];
        }
        delete[] temp;
        delete[] templ;
      }
      /* Elegxos Fortou: Pernaei mia diadikasia sto disko.
       * To pia diadikasia tha metaferthei de dieukrinizetai, etsi 
       * epilegw na perasw tin pio palia diadikasia pou den einai perasmeni idi.
       * 
       * Ksekatharizetai oti an kai o elegxos einai o pio katw, den yparxei periptwsi
       * na yparxoun perisoteres energes diadikasies apo oses xwrane stin fysiki mnimi.
       */
      temp = gener.uniform(lo, hi) / p_size + 1;
      
      //Analogiki anathesi plaisiwn:
      analog_assign = analog_ratio * (temp - lo / p_size) + min_pages;
      while ((cur_pages + analog_assign) > (mem_size / p_size)){
        for (i = 0; plr_array[i]->dumped(); i++);
        plr_array[i]->dump();
        cur_pages -= plr_array[i]->active_pages();
        ram_buf += plr_array[i]->dumped() * drsr;
        //Antitheta me tin ektelesi edw i CPU einai anagkasmeni na perimenei to disko.
      }
      amt_proc++; //could be replaced by id.
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      if((pr_array[pr_in] = new Process(temp, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      if((plr_array[pr_in] = new LRUList(analog_assign * p_size, p_size)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plr_array[pr_in]->insert(tentry);
      }
      cur_pages += analog_assign;
      plr_array[pr_in]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    if (!pr_in){ //not a normal time to generate process, but no processes alive, so time is frozen.
      time = spawn;
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      amt_proc++;
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      temp = gener.uniform(lo, hi) / p_size + 1;
      analog_assign = analog_ratio * (temp - lo / p_size) + min_pages;
      if((pr_array[pr_in] = new Process(temp, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      if((plr_array[pr_in] = new LRUList(analog_assign * p_size, p_size)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plr_array[pr_in]->insert(tentry);
      }
      
      cur_pages += analog_assign;
      plr_array[pr_in]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    //Proceeding with round-robin execution of the processes.
    frozen = 0; //Used to check if all processes are waiting for the disk.
    defrost = 0;
    for(i = 0; i < pr_in; i++){
      if (pr_array[i]->waiting()){
        frozen++;
        if (pr_array[defrost]->get_waittime() > pr_array[i]->get_waittime())
          defrost = i;
          //An den einai pagwmeni i 1i den leitourgei swsta. Alla auto de mas endiaferei.
        continue; //I process perimenei to disko.
      }
      //Checking if process is active:
      if (plr_array[i]->dumped()){
        tpfaults += 1; //i einai oses oi selides tis diergasias?
        while (cur_pages >= (mem_size / p_size + plr_array[i]->active_pages())){
          for (j = 0; plr_array[j]->dumped(); j++); if (j >= pr_in) cout << "omgwtfbbq.." << endl;
          plr_array[j]->dump();
          cur_pages -= plr_array[j]->active_pages();
          ram_buf += plr_array[j]->dumped() * drsr;
        }
        ram_buf += plr_array[i]->restore() * drsr;
        cur_pages += plr_array[i]->active_pages();
      }
        /* Thewrw ws dedomeno oti tha apothikeutoun oses selides xrisimopoiountai
         * apo ti diergasia kai oxi oses selides tis diatithentai.
         * Episis, afou i cpu anagkazetai na perimenei tin eggrafi sto disko, kai afou
         * o diskos einai pio argos apo ti RAM, autos einai pou kathysterei
         * to systima, kai me vasi tis dikes tou taxytites metrietai o xronos edw.
         */
      
      //Checking if process is active:
      temp = pr_array[i]->request(); //Page request.
      fetched = plr_array[i]->fetch(temp, pr_array[i]->get_pid());
      plr_array[i]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[i]->set_waittime(disk_temp * drsr);
      if (pr_array[i]->operate(fetched)) //ean egine allagi sta dedomena
        ram_buf += gener.exponential((lo + hi) / 2); // ekthetika katanemimeno me mesi timi ti mesi mnimi pou katalamvanei mia diadikasia.
                                                     //Thwreitai diladi, oti i mesi diadikasia tha metavalei kata meso oro elaxista ligotera apo ta misa tis.
      ram_buf += gener.exponential((lo + hi) / 2);   //I idia diadikasia gia to diavasma dedomenwn.
      plr_array[i]->update(fetched);
    }
    /* An oles oi processes perimenoun to disko apo ton proigoumeno kyklo, den yparxei kamia 
     * gia na kinisei to xrono, o opoios xronos metrietai se bytes tis RAM.
     * Gia na proxwrisw to xrono to metavallw apeutheias analoga me ti process pou
     * xreiazetai to mikrotero xrono gia na 3ekleidwthei.
     */
    if (frozen == pr_in){ //to pr_in einai != 0 edw, de xreiazetai na to eleg3oume.
      ram_buf = pr_array[defrost]->get_waittime(); //i ram_buf xrisimopoieitai san temp.
      for(i = 0; i < pr_in; i++) //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    else{
      for(i = 0; i < pr_in; i++)  //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    //End of cycle, removing dying processes and their according pages.
    temp = 0; //Used to assign shift amount, in case of deletion.
    for(i = 0; i < pr_in; i++){ // if no processes, this is not executed.
      if (pr_size - temp > i) {
        pr_array[i] = pr_array[i + temp];
        plr_array[i] = plr_array[i + temp];
      }
      if (pr_array[i]->isdead()){
        cur_pages -= plr_array[i]->active_pages();
        tpfaults +=plr_array[i]->get_pfaults();
        tpreqs += plr_array[i]->get_prequests();
        delete plr_array[i];
        delete pr_array[i];
        temp++;
        --i;
        --pr_in;
      }
    }
  }
  //Present data.
  for (i = 0; i < pr_in; i ++){
    tpfaults +=plr_array[i]->get_pfaults();
    tpreqs += plr_array[i]->get_prequests();
  }
  tproc = amt_proc;
  avg_turn = (double)total_life / amt_proc;
  avg_req_ppf = (double)tpreqs / tpfaults;
  return EXIT_SUCCESS;
}
