#ifndef CLASSESH
#define CLASSESH

#include <cstdlib>
#include <cmath>
#include <iostream>
#include "distribution.h"

using namespace std;

class pentry {
  private:
    bool is_here;
    bool clean;
    int pr_id;
    int id;
  public:
    pentry(int, int);
    pentry(bool, bool, int, int);
    bool ishere(void);
    void set_here(bool);
    bool isclean(void);
    void make_clean(void);
    void make_dirty(void);
    int owner(void);
    int get_id(void);
};

class Page {
  private:
    Page *next;
    Page *prev;
    pentry* data;
  public:
    Page(Page*, pentry*, Page*);
    ~Page(void);
    void set_data(pentry*);
    pentry* get_data(void);
    void set_next(Page*);
    Page* get_next(void);
    void set_prev(Page*);
    Page* get_prev(void);
    int compare(Page*);
};

class LRUList {
  private:
    Page *first, *last;
    int amt;
    int page_size;
    int ram_pages;
    int page_faults;
    int page_requests;
    long ram_buf;
    long disk_buf;
    long dumpe;
  public:
    LRUList(int, int);
    ~LRUList(void);
    int setAmt(int);
    int getAmt(void);
    int insert(pentry*);
    pentry* fetch(int, int);
    void rem_procmem(int);
    void update(pentry*);
    int get_pfaults(void);
    void reset_pfaults(void);
    int get_prequests(void);
    void parse_list(void);
    void flush(long&, long&);
    void dump (void);
    long dumped (void);
    long restore (void);
    int active_pages(void);
    void increase_pages(int);
    bool decrease_pages(int);
};

class Process {
  private:
    int pid;
    int size;
    long lifespan;
    long alivetime;
    long waittime;
  public:
    Process(int, long);
    int request(void);
    bool operate(pentry*);
    int get_pid(void);
    bool isdead(void);
    void age(long);
    void set_waittime(long);
    long get_waittime(void);
    bool waiting(void);
    long uptime(void);
};

class MemUnit {
private:
  LRUList *plist;
  LRUList **plr_array;
  int lo;
  int hi;
  int p_size;
  int mem_size;
  int pf_min;
  int pf_max;
  int min_pages;
  int max_pages;
  int t;
  int T;
  int sts; //simulation time in seconds
  int pr_in, pr_size;
  Process **pr_array;
public:
  MemUnit(int, int, int, int, int, int, int); //GLOBAL
  MemUnit(int, int, int, int, int, int, int, int, int);  //LOCAL / PFF
  ~MemUnit(void);
  int operate(long&, long&, long&, double&, double&);
};

#endif
