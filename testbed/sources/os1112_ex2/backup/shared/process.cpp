#include "classes.h"
#include "distribution.h"

Process::Process (int pages, long lifespan){
  static int pidassign = 0;
  pid = pidassign++;
  size = pages;
  this->lifespan = lifespan;
  alivetime = 0;
  waittime = 0;
  return;
}

int Process::request(void){
  int retval;
  Distribution gener = Distribution::instance();
  while ((retval = gener.exponential((int)sqrt(size))) >= size); //An vgei megalytero apo n prepei na epanektelestei i synartisi.
  return retval;
}

bool Process::operate(pentry *mypage){
  bool retval;
  Distribution gener = Distribution::instance();
  retval = gener.uniform(0,1) ? true : false;
  if (retval){
    mypage->make_dirty();
  }
  return retval;
}

int Process::get_pid(void){
  return pid;
}

bool Process::isdead(void){
  return ((lifespan < alivetime) ? true : false);
}

void Process::age(long modif){
  alivetime += modif;
  waittime -= modif;
  return;
}

void Process::set_waittime(long set){
  waittime = set;
  return;
}

long Process::get_waittime(void){
  return waittime;
}

bool Process::waiting(void){
  return ((waittime > 0) ? true : false);
}

long Process::uptime(void){
  return alivetime;
}