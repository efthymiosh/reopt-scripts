#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "../shared/classes.h"
#include "../shared/distribution.h"

using namespace std;

/*Η συνολική διάρκεια προσομοίωσης (D) ορίζεται σαν παράμετρος του προγράμματος. 
 * Επίσης, παράμετροι του προγράμματος είναι οι lo, hi, τ, Τ, το μέγεθος P (ΚΒ) 
 * σελίδας καθώς και το μέγεθος της μνήμης S (ΚΒ).
 */
int main(int argc, char* argv[]) {
  int lo, hi, procspawn_t, procdur_T, psize_P, memsize_S, simdur_D, iter;
  char flag;
  long tpf, tpr, tpc;
  double aptt, aprppf;
  long tot_tpf, tot_tpr, tot_tpc;
  double tot_aptt, tot_aprppf;
  MemUnit *mem_manager;
  if (argc != 8){
    cout << "Amount of arguments is different than then required amount." << endl;
    cout << "Please execute program as: " << argv[0] << " [D] [lo] [hi] [t] [T] [P] [S]" << endl;
    return EXIT_FAILURE;
  }
  iter = 0;
  tot_tpf = 0, tot_tpr = 0, tot_tpc = 0;
  tot_aptt = 0, tot_aprppf = 0;
  simdur_D = atoi(argv[1]); 
  lo = atoi(argv[2]); 
  hi = atoi(argv[3]); 
  procspawn_t = atoi(argv[4]); 
  procdur_T = atoi(argv[5]); 
  psize_P = atoi(argv[6]); 
  memsize_S = atoi(argv[7]);
  if ((simdur_D < 1) || (lo < 1) || (hi < 1) || (procspawn_t < 1) || (procdur_T < 1) || (psize_P < 1) || (memsize_S < 1)){
    cout << "All amounts must be greater than zero." << endl;
    return EXIT_FAILURE;
  }
  cout << "Please Verify data:" << endl;
  cout << "Simulation Duration D(sec) = " << simdur_D << endl;
  cout << "Process size range [lo(KB),hi(KB)]: lo = " << lo << endl;
  cout << "                                    hi = " << hi << endl;
  cout << "Mean time between process spawns t(sec) = " << procspawn_t << endl;
  cout << "Process duration mean time T(sec) = " << procdur_T << endl;
  cout << "Page Size P(KB) = " << psize_P << endl;
  cout << "Real/Physical Memory Size S(KB) = " << memsize_S << endl;  
//   cout << "Proceed? (y/n): "; //MODIFICATION: COMMENTED
//   cin >> flag;
//   if (flag != 'y')
//     return EXIT_SUCCESS;
  
  cout << endl << "Initiating simulation process. Simulating a PC with " << memsize_S << "KB RAM  with Read/Write speeds at 100MB/s," << endl
               << "on a disk with infinite size and Read/Write speeds at 5MB/s, for " << simdur_D << "secs." << endl;
//   while (iter < 1){ //MODIFICATION: COMMENTED
//     cout << "Please insert simulation iterations to complete: ";
//     cin >> iter;
//   }
  iter = 5; //MODIFICATION: INSERTED LINE
  cout << endl << "+-----------------+-------------------+-----------------------+-------------------------------+------------------------------------+"<<endl;
  cout << "|Total page faults|Total page requests|Total processes created|Average process turnaround time|Average page requests per page fault|"<<endl;
  cout << "+-----------------+-------------------+-----------------------+-------------------------------+------------------------------------+"<<endl;
  for (int i = 0; i < iter; i++){
    if ((mem_manager = new MemUnit(lo, hi, psize_P, memsize_S, simdur_D, procspawn_t, procdur_T)) == NULL){
      cerr << "Unable to allocate memory. Exiting.." << endl;
      return EXIT_FAILURE;
    }
    if (mem_manager->operate(tpf, tpr, tpc, aptt, aprppf) == EXIT_FAILURE){
      cerr << "Exiting with error.." << endl;
      return EXIT_FAILURE;
    }
    delete mem_manager;
    tot_tpf+=tpf;
    tot_tpr+=tpr;
    tot_tpc+=tpc;
    tot_aptt+=aptt;
    tot_aprppf+=aprppf;
    cout << "|" << setw(17) << tpf;
    cout << "|" << setw(19) << tpr;
    cout << "|" << setw(23) << tpc;
    cout << "|" << setw(26) << aptt << setw(5) << " secs";
    cout << "|" << setw(36) << aprppf << "|" << endl;
  }
  cout << "+-----------------+-------------------+-----------------------+-------------------------------+------------------------------------+-------+"<<endl;
  cout << "|" << setw(17) << tot_tpf / iter;
  cout << "|" << setw(19) << tot_tpr / iter;
  cout << "|" << setw(23) << tot_tpc / iter;
  cout << "|" << setw(26) << tot_aptt / iter << setw(5) << " secs";
  cout << "|" << setw(36) << tot_aprppf / iter << "|" << "Average|" << endl;
  cout << "+-----------------+-------------------+-----------------------+-------------------------------+------------------------------------+-------+"<<endl << endl;
  return EXIT_SUCCESS;
}

