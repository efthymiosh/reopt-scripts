#include "../shared/classes.h"
#include "../shared/distribution.h"

MemUnit::MemUnit(int lo, int hi, int p_size, int mem_size, int sim_dur, int t, int T, int page_min, int page_max){
  this->lo = lo;
  this->hi = hi;
  this->p_size = p_size;
  this->mem_size = mem_size;
  this->t = t;
  this->T = T;
  pf_min = page_min;
  pf_max = page_max;
  sts = sim_dur;
  if ((pr_array = new Process*[pr_size = (sim_dur / T * 4)]) == NULL){ //To megethos ypologistike empeirika. Ginetai kai elegxos.
      cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
      return;
  }
  if ((plr_array = new LRUList*[pr_size]) == NULL){
    cerr << "MemUnit::MemUnit(): Unable to allocate memory.." << endl;
    return;
  }
  pr_in = 0;
  return;
}

MemUnit::~MemUnit(void){
  while(pr_in--){
    delete pr_array[pr_in];
    delete plr_array[pr_in];
  }
  delete[] pr_array;
  delete[] plr_array;
  return;
}

int MemUnit::operate(long& tpfaults, long& tpreqs, long& tproc, double& avg_turn, double& avg_req_ppf){
  int temp, frozen, defrost, i, j, cur_pages;
  double dtemp;
  pentry *fetched;
  long long time;
  long long spawn;
  long total_life = 0;
  long amt_proc = 0;
  long drsr; //disk to ram speed ratio
  long ram_buf, disk_buf, ram_temp, disk_temp, pr_life;
  pentry *tentry;
  Distribution gener = Distribution::instance();
  tpfaults = 0;
  tpreqs = 0;
  if (pr_array == NULL){
    cerr << "MemUnit::operate(): No memory assigned to the process array" << endl;
    return 1;
  }
  if (plr_array == NULL){
    cerr << "MemUnit::operate(): No memory assigned to the LRU array" << endl;
    return 1;
  }
  if (hi > mem_size){
    cout << "MemUnit::operate(): Maximum process memory (" << hi << ") larger than total available RAM (" << mem_size << ")" << endl; //ean de mporei na xwresei tin pio megali diergasia
    return 1;
  }
  drsr = 20;
  cur_pages = 0;
  spawn = time = sts * 100 * 1024; //STS * seconds * 100 * MB / seconds = STS * 100 * 1024 * KB. O xronos metrietai se KB.
  if (spawn < 0) //overflow
    return 1;
  /*****MAIN LOOP*****/
  while (time > 0){
    ram_buf = 0;
    
    /*****PFF functionality:*****/
    for (i =0; i < pr_in; i++){
      if (pr_array[i]->uptime() <= (2 * p_size)) //ean i diergasia den exei kanei 2 kiniseis akomi, tote tin agnoei.
        continue;
      dtemp = plr_array[i]->get_pfaults() / ((double)(pr_array[i]->uptime()) / 100 / 1024); //pagefaults / sec.
      if (dtemp > pf_max){
        if (cur_pages < mem_size / p_size){
          plr_array[i]->increase_pages(1);
          cur_pages++;
        }
      }
      else if (dtemp < pf_min){
        if (plr_array[i]->decrease_pages(1))
          cur_pages--;
      }
    }
    
    /*****Time to generate new process*****/
    if (time <= spawn) { 
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      if (pr_in == pr_size) { //process array full.
        //cout << "Process array full. Doubling process array size..." << endl;
        Process **temp = pr_array;
        LRUList **templ = plr_array;
        pr_size *= 2;
        if ((pr_array = new Process*[pr_size]) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        if ((plr_array = new LRUList*[pr_size]) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        for (i = 0; i < pr_in; i++){
          pr_array[i] = temp[i];
          plr_array[i] = templ[i];
        }
        delete[] temp;
        delete[] templ;
      }
      /***** Elegxos Fortou:*****
       * An i mnimi einai gemati tote pernane diadikasies sto disko,
       * mexri na dimiourgithei arketos xwros.
       * O tropos epilogis diergasias gia ksefortwma de dieukrinizetai, etsi 
       * epilegw na 3ekinisw apo tin pio palia diadikasia pou den einai
       * perasmeni idi sto disko.
       */
      while ((cur_pages + (lo + hi) / 2 / p_size + 1) > (mem_size / p_size)){
        for (i = 0; plr_array[i]->dumped(); i++);
        plr_array[i]->dump();
        cur_pages -= plr_array[i]->active_pages();
        ram_buf += plr_array[i]->dumped() * drsr;
      }
      amt_proc++; //could be replaced by id.
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      if((pr_array[pr_in] = new Process(temp = gener.uniform(lo, hi) / p_size + 1, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      if((plr_array[pr_in] = new LRUList(((lo + hi) / 2), p_size)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      cur_pages += (lo + hi) / 2 / p_size + (lo + hi) % 2;
      /* den yparxei kapoio sygkekrimeno noima sto giati (lo + hi) / 2 tha einai o
       * arxikos arithmos diathesimis mnimis gia energa plaisia tis diadikasias.
       * Apla tha epitrepsei ston
       * PFF na kinithei sigoura kai pros ta panw kai pros ta katw kata
       * ti diarkeia tis prosomoiwsis.
       */
      
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plr_array[pr_in]->insert(tentry);
      }
      plr_array[pr_in]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    //not a normal time to generate process, but no processes alive, so time is frozen.
    if (!pr_in){ 
      time = spawn;
      spawn = time - gener.exponential(t) * 100 * 1024; //reset spawn.
      amt_proc++;
      pr_life = gener.exponential(T);
      total_life +=pr_life;
      if((pr_array[pr_in] = new Process(temp = gener.uniform(lo, hi) / p_size + 1, pr_life * 100 * 1024)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      if((plr_array[pr_in] = new LRUList(((lo + hi) / 2), p_size)) == NULL){
        cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
        return 1;
      }
      cur_pages += (lo + hi) / 2 / p_size + (lo + hi) % 2;
      for(i = 0; i < temp; i++){
        if((tentry = new pentry(pr_array[pr_in]->get_pid(),i)) == NULL){
          cerr << "MemUnit::operate(): Unable to allocate memory.." << endl;
          return 1;
        }
        plr_array[pr_in]->insert(tentry);
      }

      plr_array[pr_in]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[pr_in]->set_waittime(disk_temp * drsr);
      pr_in++;
    }
    
    //Proceeding with round-robin execution of the processes.
    
    frozen = 0; //Used to check if all processes are waiting for the disk.
    defrost = 0;
    for(i = 0; i < pr_in; i++){
      if (pr_array[i]->waiting()){
        frozen++;
        if (pr_array[defrost]->get_waittime() > pr_array[i]->get_waittime())
          defrost = i;
          //An den einai pagwmeni i 1i den leitourgei swsta. Alla auto de mas endiaferei.
        continue; //I process perimenei to disko.
      }
      
      //Checking if process is active:
      if (plr_array[i]->dumped()){
        tpfaults += 1; //i einai oses oi selides tis diergasias?
        while (cur_pages >= (mem_size / p_size) + plr_array[i]->active_pages()){
          for (j = 0; plr_array[j]->dumped(); j++);
          plr_array[j]->dump();
          cur_pages -= plr_array[j]->active_pages();
          ram_buf += plr_array[j]->dumped() * drsr;
        }
        ram_buf += plr_array[i]->restore() * drsr;
        cur_pages += plr_array[i]->active_pages();
      }
      temp = pr_array[i]->request(); //Page request.
      fetched = plr_array[i]->fetch(temp, pr_array[i]->get_pid());
      plr_array[i]->flush(ram_temp, disk_temp);
      ram_buf += ram_temp;
      if (disk_temp)
        pr_array[i]->set_waittime(disk_temp * drsr);
      if (pr_array[i]->operate(fetched)) //ean egine allagi sta dedomena
        ram_buf += gener.exponential((lo + hi) / 2); // ekthetika katanemimeno me mesi timi ti mesi mnimi pou katalamvanei mia diadikasia.
                                                     //Thwreitai diladi, oti i mesi diadikasia tha metavalei kata meso oro elaxista ligotera apo ta misa tis.
      ram_buf += gener.exponential((lo + hi) / 2);   //I idia diadikasia gia to diavasma dedomenwn.
      plr_array[i]->update(fetched);
    }
    /* An oles oi processes perimenoun to disko apo ton proigoumeno kyklo, den yparxei kamia 
     * gia na kinisei to xrono, o opoios xronos metrietai se KBytes tis RAM.
     * Gia na kinisw to xrono, diaforopoiw ti metavoli tou, apo KBytes tis RAM, se KBytes tou
     * diskou.
     */
    if (frozen == pr_in){ //to pr_in einai != 0 edw, de xreiazetai na to eleg3oume.
      ram_buf = pr_array[defrost]->get_waittime(); //i ram_buf xrisimopoieitai san temp.
      for(i = 0; i < pr_in; i++) //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    else{
      for(i = 0; i < pr_in; i++)  //reduce lifespan
        pr_array[i]->age(ram_buf);
      time -= ram_buf;
    }
    //End of cycle, removing dying processes and their according pages.
    temp = 0; //Used to assign shift amount, in case of deletion.
    for(i = 0; i < pr_in; i++){ // if no processes, this is not executed.
      if (pr_size - temp > i) {
        pr_array[i] = pr_array[i + temp];
        plr_array[i] = plr_array[i + temp];
      }
      if (pr_array[i]->isdead()){
        tpfaults +=plr_array[i]->get_pfaults();
        tpreqs += plr_array[i]->get_prequests();
        cur_pages -= plr_array[i]->active_pages();
        delete plr_array[i];
        delete pr_array[i];
        temp++;
        --i;
        --pr_in;
      }
    }
  }
  //Present data.
  for (i = 0; i < pr_in; i ++){
    tpfaults +=plr_array[i]->get_pfaults();
    tpreqs += plr_array[i]->get_prequests();
  }
  tproc = amt_proc;
  avg_turn = (double)total_life / amt_proc;
  avg_req_ppf = (double)tpreqs / tpfaults;
  return EXIT_SUCCESS;
}
