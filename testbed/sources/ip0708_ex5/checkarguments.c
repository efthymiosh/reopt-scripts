#include "functions.h"
#include "structs_functions_ext.h"

int checkarguments(int argc,char **argv){
  int returnvalue = 0, i; //synartisi pou elegxei ta orismata kai epistrefei analoga tin katallili timi sti main.
  if (argc <= 1){
    printf("Please use the crossword solving program as follows: crossword <crosswordfilename> [-check] [-dict <dictionary filename>] [-draw]\n");
    return -1;
  }
  for (i = 0; i < argc; i++){
    if (!strcmp(argv[i], "-check")){
      returnvalue += 1;
      break;
    }
  }
  for (i = 0; i < argc; i++){
    if (!strcmp(argv[i], "-draw")){
      returnvalue += 10;
      break;
    }
  }
  return returnvalue;
}

