#include "functions.h"
#include "structs_functions_ext.h"

void drawcross(int cross_size){
  int i; int j; //synartisi pou anaparista grafika to staurolekso.
  for (i = 0; i < (cross_size + 2); i++){
    for (j = 0; j < (cross_size + 2); j++){
      if (crossword[i][j].letter == '\0')
        printf("%3c", '#');
      else
        printf("%3c", crossword[i][j].letter);
    }
    printf("\n");
  }
}

tree* insertword(char* word, tree* treeptr){
  if (treeptr == NULL){ //simatodotei oti exoume ftasei se telos tou dentrou
    treeptr = malloc(sizeof(tree));
    treeptr->letter = *word;
    treeptr->left = NULL;
    treeptr->right = NULL;
    treeptr->down = NULL; 
  }
  if (treeptr->letter > *word) //an to gramma einai pio prin apo to gramma tou deikti
    treeptr->left = insertword(word, treeptr->left); //i synartisi epanalamvanetai anadromika gia ton deikti aristera
  else if (treeptr->letter == *word){
    if (*word != 0) //an o deixtis den exei ftasei sto telos, epanalamvanetai i synartisi gia to epomeno gramma, ston deikti ->down (idia leksi)
      treeptr->down = insertword(++word, treeptr->down);
  } 
  else
    treeptr->right = insertword(word, treeptr->right);
  return treeptr;
}

void mergesort(mergedata* sortdata) { //synartisi pou taksinomei ti lista.
  list* start = sortdata->start, *end, *p1, *p2, *node;
  int i, insize = 1, merges, size1, size2;
  if (sortdata->size <= 1) return;      
  do{
    p1 = start;
    start = end = NULL;
    merges = 0;
    while (p1 != NULL){
      p2 = p1;
      merges++;
      size1=0;                                                  
      for (i = 0 ; i < insize ; i++){                           
        size1++;                                                
        p2 = p2->next;                                          
        if (p2 == NULL) break;                                  
      }
      size2 = insize;                                                   
      while (size1 || (size2 && p2 != NULL)){
        if (!size1) {           
        node = p2;
        p2 = p2->next;
        size2--;
        }
        else if (!size2 || p2==NULL){
          node = p1;                                    
          p1 = p1->next;
          size1--;                                      
        }
        else if (p1->weight >= p2->weight){
          node = p1;                                    
          p1 = p1->next;
          size1--;                              
        }
        else{
          node = p2;                                    
          p2 = p2->next;                                
          size2--;                                      
        }
        if (end != NULL) end->next=node;                
        else start = node;                              
        end = node;                                     
      }
      p1 = p2;                                          
    }
    end->next = NULL;                                           
    insize <<= 1;                                                       
  } while (merges > 1);
  sortdata->start = start;                                              
  sortdata->end = end;                                          
}

void printcross(int cross_size){ //apli synartisi pou typwnei tis lekseis tou stauroleksou
  int i = 1, j = 1, flag = 0;
  for (i = 1; i <= cross_size; i++){
    for (j = 1; j <= cross_size; j++){
      while (crossword[i][j].wordlenhori){ //auto isxyei otan to crossword[i][j] einai panw se orizontia leksi.
        putchar(crossword[i][j++].letter);
        flag = 1;
      }
      if (flag){
        putchar('\n');
        flag = 0;
      }
    }
  }
  for (j = 1; j <= cross_size; j++){
    for (i = 1; i <= cross_size; i++){
      while (crossword[i][j].wordlenvert){ //auto isxyei otan to crossword[i][j] einai panw se katheti leksi.
        putchar(crossword[i++][j].letter);
        flag = 1;
      }
      if (flag){
        putchar('\n');
        flag = 0;
      }
    }
  }
  return;
}

void removewordhori(int wordnum, int cross_size){ //afairesi orizontias leksis.
  int i, j;
  for (i = 1; i <= cross_size; i++)
    for (j = 1; j <= cross_size; j++)
     if (crossword[i][j].orderhori == wordnum){
        crossword[i][j].orderhori = -1;
        if (crossword[i][j].ordervert == -1)
          crossword[i][j].letter = ' ';
    }
  return;
}

void removewordvert(int wordnum, int cross_size){ //afairesi kathetis leksis.
  int i, j;
  for (i = 1; i <= cross_size; i++)
    for (j = 1; j <= cross_size; j++)
      if (crossword[i][j].ordervert == wordnum){
        crossword[i][j].ordervert = -1;
        if (crossword[i][j].orderhori == -1)
          crossword[i][j].letter = ' ';
      }
  return;
}

int seekwordhori(int wordnum, int crossingword, int cross_size){
  int i, j, imax = 0, jmax = 0, notfound = 0;
  list* listptr;
  char buffer[128];
  do{
    for (i = cross_size; i >= 1; i--){
      for (j = cross_size; j >= 1; j--)
        if (crossword[i][j].letter && (crossword[i][j].ordervert == crossingword) && (crossword[i][j].orderhori == -1)){
          if (crossword[imax][jmax].wordlenhori < crossword[i][j].wordlenhori){
            notfound = 0;
            imax = i;
            jmax = j;
          }
        }
    }
    if (!imax){ //se auti tin periptwsi de vrike leksi, opote to for teliwse me to imax 0.
                //synexizei o elegxos gia pio mikri katheto mexri na vrei.
      notfound = 1;
      crossingword--;
    }
  }while (notfound && (crossingword >= -1));
  if (crossingword < -1)
    return 2;
  i = imax;
  j = jmax;
  while (crossword[i][--j].letter != '\0'); //den yparxei periptwsi na vgei eksw apo ta oria dioti to staurolekso einai perikleismeno se midenika
  j++; //o vrogxos kai to j++; fernoun tis syntetagmenes sto prwto gramma tis leksis i opoia tha mpei.
  if (pointers[wordnum] != NULL){
    if ((pointers[wordnum] = pointers[wordnum]->next) == NULL){
      freemem_list(wordsarray[wordnum]); //an den yparxoun alles diathesimes lekseis, tote eleutherwnetai i mnimi kai epistrefetai 0.
      wordsarray[wordnum] = NULL; //o pointers[wordnum] idi deixnei NULL.
      return 0;
    }
    imax = 0; //edw to imax xrisimopoieitai apla san metritis.
    while (crossword[i][j].letter = pointers[wordnum]->word[imax++])//to j den auksanetai edw dioti prepei na mpei kai i pliroforia pio katw sto idio keli.
      crossword[i][j++].orderhori = wordnum;          //to j auksanetai tin idia stigmi gia na proxwrisei o pio panw elegxos
    return 1;
  }
  imax = 0; //ta imax kai j max xrisimopoiountai apla san metrites.
  jmax = j;
  while(buffer[imax++] = crossword[i][jmax++].letter); //Ston buffer grafetai kai to '\0'.
  notfound = 0;
  checkfit(thetree[strlen(buffer) - 2], buffer, &notfound); //ginetai elegxos an sto leksiko perilamvanetai estw kai mia leksi pou sximatizetai apo ton buffer
  if (!notfound)
    return 0;
  buildlist(buffer, &wordsarray[wordnum]);
  //preparesort(wordsarray[wordnum]);
  //mergesort(&sortdata);
  pointers[wordnum] = wordsarray[wordnum];// = sortdata.start;
  imax = 0;
  while (crossword[i][j].letter = pointers[wordnum]->word[imax++]) //eggrafi tis leksis sto staurolekso.
    crossword[i][j++].orderhori = wordnum;
  return 1;
}

int seekwordvert(int wordnum, int crossingword, int cross_size){
  int i, j, imax = 0, jmax = 0, notfound = 0;
  list* listptr;
  char buffer[128];
  do{
    for (i = cross_size; i >= 1; i--){
      for (j = cross_size; j >= 1; j--)
        if (crossword[i][j].letter && (crossword[i][j].orderhori == crossingword) && (crossword[i][j].ordervert == -1)){
          if (crossword[imax][jmax].wordlenvert < crossword[i][j].wordlenvert){
            notfound = 0;
            imax = i; //ta imax, jmax arxikopoiountai me 0,  
            jmax = j; //ara stin arxi to crossword[imax][jmax].wordlenhori/vert einai 0
          }
        }
    }
    if (!imax){ //se auti tin periptwsi de vrike leksi, opote to for teliwse me to imax 0.
                //synexizei o elegxos gia pio mikri katheto mexri na vrei.
      notfound = 1;
      crossingword--;
    }
  }while (notfound && (crossingword >= -1));
  if (crossingword < -1)
    return 2;
  i = imax;
  j = jmax;
  while (crossword[--i][j].letter != '\0');
  i++; //o vrogxos fernei tis syntetagmenes sto prwto gramma tis leksis i opoia tha mpei.
  if (pointers[wordnum] != NULL){
    if ((pointers[wordnum] = pointers[wordnum]->next) == NULL){
      freemem_list(wordsarray[wordnum]);
      wordsarray[wordnum] = NULL; //o pointers[wordnum] idi deixnei NULL.
      return 0;
    }
    imax = 0; //edw to imax xrisimopoieitai apla san metritis.
    while (crossword[i][j].letter = pointers[wordnum]->word[imax++])//to j den auksanetai edw dioti prepei na mpei kai i pliroforia pio katw sto idio keli.
      crossword[i++][j].ordervert = wordnum;          //to j auksanetai tin idia stigmi gia na proxwrisei o pio panw elegxos
    return 1;
  }
  jmax = 0; //ta imax kai j max xrisimopoiountai apla san metrites.
  imax = i;
  while(buffer[jmax++] = crossword[imax++][j].letter); //Ston buffer grafetai kai to '\0'.
  notfound = 0;
  checkfit(thetree[strlen(buffer) - 2], buffer, &notfound); //ginetai elegxos an sto leksiko perilamvanetai estw kai mia leksi pou sximatizetai apo ton buffer
  if (!notfound)
    return 0;
  buildlist(buffer, &wordsarray[wordnum]);
  //preparesort(wordsarray[wordnum]);
  //mergesort(&sortdata);
  pointers[wordnum] = wordsarray[wordnum]; // = sortdata.start;
  imax = 0;
  while (crossword[i][j].letter = pointers[wordnum]->word[imax++]) //eggrafi tis leksis sto staurolekso.
    crossword[i++][j].ordervert = wordnum;
  return 1;
}