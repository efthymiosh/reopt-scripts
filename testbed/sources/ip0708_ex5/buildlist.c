#include "functions.h"
#include "structs_functions_ext.h"

void buildlist(char* word, list** listpointer){ //synartisi pou pairnei ena periorismo se lekseis(px "  p   ") kai xtizei mia lista
  int length, check = 0, i; //me lekseis pou pliroun tis proypotheseis autou tou periorismou.
  char* spacecheck, *wordtemp;
  list* pointer = *listpointer;
  length = strlen(word);
  wordtemp = malloc((length + 1) * sizeof(char));
  strcpy(wordtemp, word);
  if (spacecheck = strchr(wordtemp, ' ')){ //an o deixtis den einai NULL, diladi vrethike keno..
    for (i = 'a'; i <= 'z'; i++){
      *spacecheck = i;    //..ginetai antikatastasi me gramma kai anadromiki klisi tis synartisis.
      checkfit(thetree[length - 2], wordtemp, &check);
      if (!check)  //ean den yparxei estw mia leksi pou plirei tis proypotheseis, allazei to gramma.
        continue;  //an den yparxei to meros tis leksis prin to prwto keno, epistrefei 0, kai etsi glytwnetai para polys xronos.
      buildlist(wordtemp, listpointer);
      check = 0; //to check midenizetai 3ana, gia na mporei na ginei elegxos gia tis epomenes lekseis.
    }
    free(wordtemp);
    return;
  }
  checkfit(thetree[length - 2], word, &check); //gia na ftasei edw simainei den mpike sto proigoumeno skelos, ara to check einai akomi 0.
  if (check){ //ean yparxei i leksi..
    if (pointer != NULL){
      while (pointer->next != NULL)
        pointer = pointer->next;
      pointer->next = malloc(sizeof(list));
      pointer = pointer->next;
      pointer->word = malloc((length + 1) * sizeof(char));
      strcpy(pointer->word, word);
      pointer->next = NULL;
    }
    else{
      (*listpointer) = malloc(sizeof(list));
      (*listpointer)->word = malloc((length + 1) * sizeof(char));
      strcpy((*listpointer)->word, word);
      (*listpointer)->next = NULL;
    }
  }
  free(wordtemp);
  return;
}
