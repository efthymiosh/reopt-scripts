#include "functions.h"
#include "structs_functions_ext.h"

int buildcells(char *string, int* array_size, int* wordsum){ //synartisi pou "xtizei" ta kelia tou stauroleksou
  FILE* fpointer;
  int i,j, cross_size, blackhor, blackver;
  int tempjhori, wordcounthori = 0, tempjvert, wordcountvert = 0, temp;
  if ((fpointer = fopen(string, "r")) == NULL){
    perror(string);
    return 0;
  }
  fscanf(fpointer, "%d\n", &cross_size);  //diavazetai to megethos tou stauroleksou
  *array_size = cross_size;
  crossword = malloc((cross_size+2)*sizeof(cell*));  //stin synexeia ginetai desmeusi kai midenismos tou pinaka.
  for (i = 0; i < (cross_size+2); i++)
    crossword[i] = malloc((cross_size+2)*sizeof(cell));
  for (i = 0; i < (cross_size+2); i++)
    for (j = 0; j < (cross_size+2); j++){
      if (!i || !j || (j == cross_size + 1) || (i == cross_size + 1))
        crossword[i][j].letter = '\0';
      else
        crossword[i][j].letter = ' ';
      crossword[i][j].wordlenhori = 0;
      crossword[i][j].wordlenvert = 0;
      crossword[i][j].orderhori = -1; //To 0 einai i prwti le3i, opote auta midenizontai me -1
      crossword[i][j].ordervert = -1;
    }
  while (!feof(fpointer)){
    fscanf(fpointer, "%d %d\n", &blackhor, &blackver);
    crossword[blackhor][blackver].letter = '\0'; //to staurolekso ksekina apo to 1,1 dioti oi prwtes kai teleutaies stiles kai grammes einai desmeumenes kai midenismenes.
  }
  for (i = 0; i < cross_size + 2; i++){ //dis-diplos vrogxos pou metra parallila tis lekseis orizontia kai katheta
    tempjhori = 0;                      //ta stauroleksa einai tetragona, ara den yparxei kanena provlima.
    tempjvert = 0;
    for (j = 0; j < cross_size + 2; j++){
      if (crossword[i][j].letter == '\0'){
        if ((j - tempjhori) > 2){
          wordcounthori++;
          for (temp = tempjhori + 1; temp < j; temp++)
            crossword[i][temp].wordlenhori = j - tempjhori - 1;
        }
        tempjhori = j;
      }
      if (crossword[j][i].letter == '\0'){
        if ((j - tempjvert) > 2){
          wordcountvert++;
          for (temp = tempjvert + 1; temp < j; temp++)
            crossword[temp][i].wordlenvert = j - tempjvert - 1;
        }
        tempjvert = j;
      }
    }  
  }
  *wordsum = (wordcountvert + wordcounthori); //epistrefetai to synolo twn leksewn
  fclose(fpointer);
  return 1;
}

int buildcrossword(int cross_size , int wordsum){ //kyria synartisi tis symplirwsis leksewn sto staurolekso.
  int i,j,check, seekfunct, crossing, completeh = 0, completev = 0, backtrackings = 0;
  wordsarray = malloc (wordsum * sizeof(list*));
  pointers = malloc (wordsum * sizeof(list*));
  for (check = 0; check < wordsum; check ++){
    wordsarray[check] = NULL;
    pointers[check] = NULL;
  }
  check = 0;
  crossing = -1;
  while (check < wordsum){
    if (backtrackings >= 50000){ //an ta backtrackings exoun ftasei sto anwtato orio, o pinakas 3anamidenizetai
      for (i = 0; i < (cross_size+2); i++)
        for (j = 0; j < (cross_size+2); j++){
          if (crossword[i][j].orderhori != -1){
            crossword[i][j].orderhori = -1;
            crossword[i][j].letter = ' ';
          }
          if (crossword[i][j].ordervert != -1){
            crossword[i][j].ordervert = -1;
            crossword[i][j].letter = ' ';
          }
        }
        for (backtrackings = 1; backtrackings < wordsum; backtrackings++){ //kai san prwti leksi epilegetai i epomeni apo to leksiko
          freemem_list(wordsarray[backtrackings]);  //dld parameni anepafi kai i seekword synartisi pairnei tin epomeni automata.
          pointers[backtrackings] = wordsarray[backtrackings] = NULL;
        }
        check = 0;
        crossing = -1;
        backtrackings = 0;
    }
    if ((check % 2 && !completeh) || completev){ // oi orizonties kai kathetes proxwroun enalla3, extos an to staurolekso exei symplirwthei katheta i orizontia
      if ((seekfunct = seekwordhori(check, crossing, cross_size)) == 1){ //kaleitai i synartisi pou vriskei kai topothetei lekseis.
        crossing++;
        check++;
      }
      else if (!seekfunct){
        removewordvert((check - 1), cross_size);
        removewordhori(check, cross_size);
        crossing--;
        check--;
        backtrackings++;
      } //an den vrethike leksi, afairei tin proigoumeni, kai trexei ton algorithmo gia tin proigoumeni
        //synexizontas apo tin epomeni leksi (auti pou mpike einai auti stin opoia o deiktis wordsarray[check - 1] deixnei)
        //opote o algorithmos tha ksekinisei apo ekei kai tha parei tin epomeni.
      else{ //seekfunct == 2
        check++;
        crossing++;
        completeh = 1;
      }
    }
    else{ //ta idia epanalamvanontai kai gia kathetes lekseis.
      if ((seekfunct = seekwordvert(check, crossing, cross_size)) == 1){
        crossing++;
        check++;
      }
      else if (!seekfunct){
        removewordhori((check - 1), cross_size);
        removewordvert(check, cross_size);
        backtrackings++;
        crossing--;
        check--;
      }
      else{ //seekfunct == 2
        check++;
        crossing++;
        completev = 1;
      }
    }
  }
}

void buildlist(char* word, list** listpointer){ //synartisi pou pairnei ena periorismo se lekseis(px "  p   ") kai xtizei mia lista
  int length, check = 0, i; //me lekseis pou pliroun tis proypotheseis autou tou periorismou.
  char* spacecheck, *wordtemp;
  list* pointer = *listpointer;
  length = strlen(word);
  wordtemp = malloc((length + 1) * sizeof(char));
  strcpy(wordtemp, word);
  if (spacecheck = strchr(wordtemp, ' ')){ //an o deixtis den einai NULL, diladi vrethike keno..
    for (i = 'a'; i <= 'z'; i++){
      *spacecheck = i;    //..ginetai antikatastasi me gramma kai anadromiki klisi tis synartisis.
      checkfit(thetree[length - 2], wordtemp, &check);
      if (!check)  //ean den yparxei estw mia leksi pou plirei tis proypotheseis, allazei to gramma.
        continue;  //an den yparxei to meros tis leksis prin to prwto keno, epistrefei 0, kai etsi glytwnetai para polys xronos.
      buildlist(wordtemp, listpointer);
      check = 0; //to check midenizetai 3ana, gia na mporei na ginei elegxos gia tis epomenes lekseis.
    }
    free(wordtemp);
    return;
  }
  checkfit(thetree[length - 2], word, &check); //gia na ftasei edw simainei den mpike sto proigoumeno skelos, ara to check einai akomi 0.
  if (check){ //ean yparxei i leksi..
    if (pointer != NULL){
      while (pointer->next != NULL)
        pointer = pointer->next;
      pointer->next = malloc(sizeof(list));
      pointer = pointer->next;
      pointer->word = malloc((length + 1) * sizeof(char));
      strcpy(pointer->word, word);
      pointer->next = NULL;
    }
    else{
      (*listpointer) = malloc(sizeof(list));
      (*listpointer)->word = malloc((length + 1) * sizeof(char));
      strcpy((*listpointer)->word, word);
      (*listpointer)->next = NULL;
    }
  }
  free(wordtemp);
  return;
}

int buildtree(char* dictfile, int maxsize){ //synartisi pou xtizei ta dentra eyresis leksewn.
  FILE* dictpointer; //dimiourgeitai ena dentro ana megethos leksis.
  char buffer[1024], *newlinecheck;
  int i, j, wordsize;
//  for (i = 0; i < 26; i++)  //o ekswterikos pinakas freqarray midenizetai.
//    freqtable[i] = 0;
  if ((dictpointer = fopen(dictfile, "r")) == NULL){
    perror(dictfile);
    return 0;
  }
  thetree = malloc((maxsize - 1)  * sizeof(tree*));
  for (i = 0; i < (maxsize - 1); i++)
    thetree[i] = NULL;
  while (fgets(buffer, 1024, dictpointer) != NULL){
    if ((newlinecheck = strrchr(buffer, '\n')) != NULL)
      *newlinecheck = 0; //an diavastike allagi grammis (kanonika se oles ektos apo isws tin teleutaia) auti midenizetai.
    wordsize = strlen(buffer);
    if (wordsize > maxsize)
      continue;
//    for (i = 0; buffer[i]; i++)  //o elegxos syxnotitas ginetai meta tin aporipsi twn megalwn leksewn wste na einai oso to dynato pio akrivis.
//      freqtable[buffer[i] - 'a']++;
    thetree[wordsize - 2] = insertword(buffer, thetree[wordsize - 2]);
  }
  return 1;
}

int freeall(int cross_size, int wordsum){
  int i;
  for (i = 0; i < wordsum; i++)
    freemem_list(wordsarray[i]);
  free(wordsarray); //o logos pou i wordsarray einai orismeni san ekswteriki.
  free(pointers);
  for (i = 0; i < cross_size + 2; i++)
    free(crossword[i]);
  free(crossword);
  for (i = 0; i < cross_size - 1; i++) //mexri cross_size -1. thesi 0: lekseis 2grammatwn, thesi cross_size - 2: lekseis cross_size grammatwn
    freetree(thetree[i]);
  free(thetree);
}

void freemem_list(list* listpointer){ //synartisi pou eleutherwnei mia lista.
  if (listpointer != NULL){
    freemem_list(listpointer->next);
    free(listpointer);
  }
  return;
}

void freetree(tree* pointer){
  if (pointer == NULL)
    return;
  freetree(pointer->right);
  freetree(pointer->down);
  freetree(pointer->left);
  free(pointer);
  return;
}

void checkfit(tree* pointer, char* word, int *count){ //synartisi pou metra poses fores synantatai ena meros mias leksis sto leksiko.
   if (!pointer) //kathe fora pou vriskei mia leksi, prosthetei 1 sto periexomeno tou count, to opoio dinetai san dieuthynsi enos int
     return; //apo tis synartiseis pou tin kaloun
  if (*word == ' ' || *word < pointer->letter)
    checkfit(pointer->left, word, count);
  if (*word == ' ' || *word == pointer->letter)
    if (pointer->letter && *word)
      checkfit(pointer->down, word + 1, count);
  if (*word == ' ' || *word > pointer->letter)
    checkfit(pointer->right, word, count);
  if (*word == 0 && pointer->letter == 0)
    (*count)++;
  return;
}

int checkarguments(int argc,char **argv){
  int returnvalue = 0, i; //synartisi pou elegxei ta orismata kai epistrefei analoga tin katallili timi sti main.
  if (argc <= 1){
    printf("Please use the crossword solving program as follows: crossword <crosswordfilename> [-check] [-dict <dictionary filename>] [-draw]\n");
    return -1;
  }
  for (i = 0; i < argc; i++){
    if (!strcmp(argv[i], "-check")){
      returnvalue += 1;
      break;
    }
  }
  for (i = 0; i < argc; i++){
    if (!strcmp(argv[i], "-draw")){
      returnvalue += 10;
      break;
    }
  }
  return returnvalue;
}

int checkwords(int cross_size){ //synartisi pou kaleitai an yparxei to -check argument.
  char buffer[1024];            //geniki leitourgia: prospelazei to staurolekso vazontas tis lekseis mia mia orizontia
  int i = 1, j = 1, bufcount = 0, check = 0, count = 0; //molis symplirwthoun oi orizonties, pairnei tis kathetes mia mia
  while (!crossword[i][j].wordlenhori){ //kai elegxei an tairiazoun. Typwnei ta katallila minimata lathous an to staurolekso de symplirwthei swsta.
    j++;
    if (j > cross_size){
      j = 1;
      i++;
    }
    if (i > cross_size)
      break;
  }
  while (i <= cross_size){
    if (fgets(buffer, 1024, stdin) == NULL){
      printf("Not enough words\n");
      return 0;
    }
    *strrchr(buffer, '\n') = '\0'; //de xreiazetai elegxos giauto dioti to programma panta eksagei '\n' meta apo kathe leksi
    check = 0;
    checkfit(thetree[strlen(buffer) - 2], buffer, &check);
    if (!check){
      printf("Word \"%s\" does not exist in the dictionary, quitting\n", buffer);
      return 0;
    }
    count = 0;
    if (crossword[i][j].wordlenhori == strlen(buffer)){
      while (buffer[count] != '\0')
        crossword[i][j++].letter = buffer[count++];
      while (crossword[i][j].letter == '\0'){
        j++;
        if (j > cross_size){
          j = 1;
          i++;
        }
        if (i > cross_size){
          break;
        }
      }  
    }
    else{
      printf("Word \"%s\" is not equal in length to the size of word expected (%d) for this slot\n", buffer, crossword[i][j].wordlenhori);
      return 0;
    }
  }
  i = j = 1;
  while (!crossword[i][j].wordlenvert){
    i++;
    if (i > cross_size){
      i = 1;
      j++;
    }
    if (j > cross_size)
      break;
  }
  while (j <= cross_size){
    if (fgets(buffer, 1024, stdin) == NULL){
      printf("Not enough words\n");
      return 0;
    }
    *strrchr(buffer, '\n') = '\0'; //de xreiazetai elegxos giauto dioti to programma panta eksagei '\n' meta apo kathe leksi
    check = 0;
    checkfit(thetree[strlen(buffer) - 2], buffer, &check);
    if (!check){
      printf("Word \"%s\" does not exist in the dictionary. Quitting...\n", buffer);
      return 0;
    }
    count = 0;
    if (crossword[i][j].wordlenvert == strlen(buffer)){
      while (buffer[count] != '\0'){
        if (crossword[i][j].letter == buffer[count]){
          i++;
          count++;
        }
        else{
          printf("Vertical word formed by horizontal words input before does not match given vertical word (\"%s\"). Quitting...\n", buffer);
          return 0;
        }
      }
      while (crossword[i][j].letter == '\0'){
        i++;
        if (i > cross_size){
          i = 1;
          j++;
        }
        if (j > cross_size)
          break;
      }  
    }
    else{
      printf("Word \"%s\" is not equal in length to the size of word expected (%d) for this slot\n", buffer, crossword[i][j].wordlenvert);
      return 0;
    }
  }
  if (getchar() != EOF){
    printf("More words than needed\n");
    return 0;
  }
  return 1;
}
