#include <stdio.h>
#include <stdlib.h>
#include <string.h> //auta perilamvanoun ta dika tous ifndef define. an perilifthoun isws ginei kapoio mperdema.
                    //dioti den tha exoun orismenes se oles tis synartiseis tis dikes tous pou isws xreiazontai.
//#ifndef MYFUNCTIONSHEADER
//#define MYFUNCTIONSHEADER
void drawcross(int);
void printcross(int);
int buildcells(char*, int*, int*);
int buildcrossword(int, int);
int buildtree(char*, int);
int checkarguments(int, char**);
void removewordhori(int wordnum, int cross_size);
void removewordvert(int wordnum, int cross_size);
int seekwordhori(int, int, int);
int seekwordvert(int, int, int);
int checkwords(int);
int freeall(int, int);

//int freqtable[26]; //ekswterikos pinakas emfanisis syxnotitwn twn grammatwn, dedomena s'auton grafontai  mia fora mono.
                   //i synartisi pou ton grafei den kalei tin synartisi pou ton diavazei, opote tha xreiazotan:
                   //na perna apo tin buildtree stin main, apo ti main stin buildcrossword, apo tin buildcrossword stin seekword kai
                   //apo tin seekword stin preparesort, opou i teleutaia kai mono prospelazei ta dedomena.
//den xrisimopoieitai, einai o pinakas pou apothikeuei to poses fores yparxei to kathe gramma sto leksiko, gia to mergesorting.
//#endif
