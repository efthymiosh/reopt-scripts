#include "functions.h"
#include "structs_functions_ext.h"

int buildcells(char *string, int* array_size, int* wordsum){ //synartisi pou "xtizei" ta kelia tou stauroleksou
  FILE* fpointer;
  int i,j, cross_size, blackhor, blackver;
  int tempjhori, wordcounthori = 0, tempjvert, wordcountvert = 0, temp;
  if ((fpointer = fopen(string, "r")) == NULL){
    perror(string);
    return 0;
  }
  fscanf(fpointer, "%d\n", &cross_size);  //diavazetai to megethos tou stauroleksou
  *array_size = cross_size;
  crossword = malloc((cross_size+2)*sizeof(cell*));  //stin synexeia ginetai desmeusi kai midenismos tou pinaka.
  for (i = 0; i < (cross_size+2); i++)
    crossword[i] = malloc((cross_size+2)*sizeof(cell));
  for (i = 0; i < (cross_size+2); i++)
    for (j = 0; j < (cross_size+2); j++){
      if (!i || !j || (j == cross_size + 1) || (i == cross_size + 1))
        crossword[i][j].letter = '\0';
      else
        crossword[i][j].letter = ' ';
      crossword[i][j].wordlenhori = 0;
      crossword[i][j].wordlenvert = 0;
      crossword[i][j].orderhori = -1; //To 0 einai i prwti le3i, opote auta midenizontai me -1
      crossword[i][j].ordervert = -1;
    }
  while (!feof(fpointer)){
    fscanf(fpointer, "%d %d\n", &blackhor, &blackver);
    crossword[blackhor][blackver].letter = '\0'; //to staurolekso ksekina apo to 1,1 dioti oi prwtes kai teleutaies stiles kai grammes einai desmeumenes kai midenismenes.
  }
  for (i = 0; i < cross_size + 2; i++){ //dis-diplos vrogxos pou metra parallila tis lekseis orizontia kai katheta
    tempjhori = 0;                      //ta stauroleksa einai tetragona, ara den yparxei kanena provlima.
    tempjvert = 0;
    for (j = 0; j < cross_size + 2; j++){
      if (crossword[i][j].letter == '\0'){
        if ((j - tempjhori) > 2){
          wordcounthori++;
          for (temp = tempjhori + 1; temp < j; temp++)
            crossword[i][temp].wordlenhori = j - tempjhori - 1;
        }
        tempjhori = j;
      }
      if (crossword[j][i].letter == '\0'){
        if ((j - tempjvert) > 2){
          wordcountvert++;
          for (temp = tempjvert + 1; temp < j; temp++)
            crossword[temp][i].wordlenvert = j - tempjvert - 1;
        }
        tempjvert = j;
      }
    }  
  }
  *wordsum = (wordcountvert + wordcounthori); //epistrefetai to synolo twn leksewn
  fclose(fpointer);
  return 1;
}
