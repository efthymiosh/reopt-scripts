#include "functions.h"
#include "structs_functions_ext.h"

void mergesort(mergedata* sortdata) { //synartisi pou taksinomei ti lista.
  list* start = sortdata->start, *end, *p1, *p2, *node;
  int i, insize = 1, merges, size1, size2;
  if (sortdata->size <= 1) return;	
  do{
    p1 = start;
    start = end = NULL;
    merges = 0;
    while (p1 != NULL){
      p2 = p1;
      merges++;
      size1=0;							
      for (i = 0 ; i < insize ; i++){				
        size1++;						
        p2 = p2->next;						
        if (p2 == NULL) break;					
      }
      size2 = insize;							
      while (size1 || (size2 && p2 != NULL)){
        if (!size1) {		
        node = p2;
        p2 = p2->next;
        size2--;
        }
        else if (!size2 || p2==NULL){
          node = p1;					
          p1 = p1->next;
          size1--;					
        }
        else if (p1->weight >= p2->weight){
          node = p1;					
          p1 = p1->next;
          size1--;				
        }
        else{
          node = p2;					
          p2 = p2->next;				
          size2--;					
        }
        if (end != NULL) end->next=node;		
        else start = node;				
        end = node;					
      }
      p1 = p2;						
    }
    end->next = NULL;						
    insize <<= 1;							
  } while (merges > 1);
  sortdata->start = start;						
  sortdata->end = end;						
}

