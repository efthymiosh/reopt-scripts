#include "functions.h"
#include "structs_functions_ext.h"

int buildtree(char* dictfile, int maxsize){ //synartisi pou xtizei ta dentra eyresis leksewn.
  FILE* dictpointer; //dimiourgeitai ena dentro ana megethos leksis.
  char buffer[1024], *newlinecheck;
  int i, j, wordsize;
//  for (i = 0; i < 26; i++)  //o ekswterikos pinakas freqarray midenizetai.
//    freqtable[i] = 0;
  if ((dictpointer = fopen(dictfile, "r")) == NULL){
    perror(dictfile);
    return 0;
  }
  thetree = malloc((maxsize - 1)  * sizeof(tree*));
  for (i = 0; i < (maxsize - 1); i++)
    thetree[i] = NULL;
  while (fgets(buffer, 1024, dictpointer) != NULL){
    if ((newlinecheck = strrchr(buffer, '\n')) != NULL)
      *newlinecheck = 0; //an diavastike allagi grammis (kanonika se oles ektos apo isws tin teleutaia) auti midenizetai.
    wordsize = strlen(buffer);
    if (wordsize > maxsize)
      continue;
//    for (i = 0; buffer[i]; i++)  //o elegxos syxnotitas ginetai meta tin aporipsi twn megalwn leksewn wste na einai oso to dynato pio akrivis.
//      freqtable[buffer[i] - 'a']++;
    thetree[wordsize - 2] = insertword(buffer, thetree[wordsize - 2]);
  }
  return 1;
}
