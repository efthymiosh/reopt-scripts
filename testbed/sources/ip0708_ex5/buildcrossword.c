#include "functions.h"
#include "structs_functions_ext.h"

int buildcrossword(int cross_size , int wordsum){ //kyria synartisi tis symplirwsis leksewn sto staurolekso.
  int i,j,check, seekfunct, crossing, completeh = 0, completev = 0, backtrackings = 0;
  wordsarray = malloc (wordsum * sizeof(list*));
  pointers = malloc (wordsum * sizeof(list*));
  for (check = 0; check < wordsum; check ++){
    wordsarray[check] = NULL;
    pointers[check] = NULL;
  }
  check = 0;
  crossing = -1;
  while (check < wordsum){
    if (backtrackings >= 50000){ //an ta backtrackings exoun ftasei sto anwtato orio, o pinakas 3anamidenizetai
      for (i = 0; i < (cross_size+2); i++)
        for (j = 0; j < (cross_size+2); j++){
          if (crossword[i][j].orderhori != -1){
            crossword[i][j].orderhori = -1;
            crossword[i][j].letter = ' ';
          }
          if (crossword[i][j].ordervert != -1){
            crossword[i][j].ordervert = -1;
            crossword[i][j].letter = ' ';
          }
        }
        for (backtrackings = 1; backtrackings < wordsum; backtrackings++){ //kai san prwti leksi epilegetai i epomeni apo to leksiko
          freemem_list(wordsarray[backtrackings]);  //dld parameni anepafi kai i seekword synartisi pairnei tin epomeni automata.
          pointers[backtrackings] = wordsarray[backtrackings] = NULL;
        }
        check = 0;
        crossing = -1;
        backtrackings = 0;
    }
    if ((check % 2 && !completeh) || completev){ // oi orizonties kai kathetes proxwroun enalla3, extos an to staurolekso exei symplirwthei katheta i orizontia
      if ((seekfunct = seekwordhori(check, crossing, cross_size)) == 1){ //kaleitai i synartisi pou vriskei kai topothetei lekseis.
        crossing++;
        check++;
      }
      else if (!seekfunct){
        removewordvert((check - 1), cross_size);
        removewordhori(check, cross_size);
        crossing--;
        check--;
        backtrackings++;
      } //an den vrethike leksi, afairei tin proigoumeni, kai trexei ton algorithmo gia tin proigoumeni
        //synexizontas apo tin epomeni leksi (auti pou mpike einai auti stin opoia o deiktis wordsarray[check - 1] deixnei)
        //opote o algorithmos tha ksekinisei apo ekei kai tha parei tin epomeni.
      else{ //seekfunct == 2
        check++;
        crossing++;
        completeh = 1;
      }
    }
    else{ //ta idia epanalamvanontai kai gia kathetes lekseis.
      if ((seekfunct = seekwordvert(check, crossing, cross_size)) == 1){
        crossing++;
        check++;
      }
      else if (!seekfunct){
        removewordhori((check - 1), cross_size);
        removewordvert(check, cross_size);
        backtrackings++;
        crossing--;
        check--;
      }
      else{ //seekfunct == 2
        check++;
        crossing++;
        completev = 1;
      }
    }
  }
}
