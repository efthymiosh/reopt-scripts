#include "functions.h"
#include "structs_functions_ext.h"

void printcross(int cross_size){ //apli synartisi pou typwnei tis lekseis tou stauroleksou
  int i = 1, j = 1, flag = 0;
  for (i = 1; i <= cross_size; i++){
    for (j = 1; j <= cross_size; j++){
      while (crossword[i][j].wordlenhori){ //auto isxyei otan to crossword[i][j] einai panw se orizontia leksi.
        putchar(crossword[i][j++].letter);
        flag = 1;
      }
      if (flag){
        putchar('\n');
        flag = 0;
      }
    }
  }
  for (j = 1; j <= cross_size; j++){
    for (i = 1; i <= cross_size; i++){
      while (crossword[i][j].wordlenvert){ //auto isxyei otan to crossword[i][j] einai panw se katheti leksi.
        putchar(crossword[i++][j].letter);
        flag = 1;
      }
      if (flag){
        putchar('\n');
        flag = 0;
      }
    }
  }
  return;
}
