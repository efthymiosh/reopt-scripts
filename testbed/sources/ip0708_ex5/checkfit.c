#include "functions.h"
#include "structs_functions_ext.h"

void checkfit(tree* pointer, char* word, int *count){ //synartisi pou metra poses fores synantatai ena meros mias leksis sto leksiko.
   if (!pointer) //kathe fora pou vriskei mia leksi, prosthetei 1 sto periexomeno tou count, to opoio dinetai san dieuthynsi enos int
     return; //apo tis synartiseis pou tin kaloun
  if (*word == ' ' || *word < pointer->letter)
    checkfit(pointer->left, word, count);
  if (*word == ' ' || *word == pointer->letter)
    if (pointer->letter && *word)
      checkfit(pointer->down, word + 1, count);
  if (*word == ' ' || *word > pointer->letter)
    checkfit(pointer->right, word, count);
  if (*word == 0 && pointer->letter == 0)
    (*count)++;
  return;
}
