#include "functions.h"
#include "structs_functions_ext.h"

int seekwordhori(int wordnum, int crossingword, int cross_size){
  int i, j, imax = 0, jmax = 0, notfound = 0;
  list* listptr;
  char buffer[128];
  do{
    for (i = cross_size; i >= 1; i--){
      for (j = cross_size; j >= 1; j--)
        if (crossword[i][j].letter && (crossword[i][j].ordervert == crossingword) && (crossword[i][j].orderhori == -1)){
          if (crossword[imax][jmax].wordlenhori < crossword[i][j].wordlenhori){
            notfound = 0;
            imax = i;
            jmax = j;
          }
        }
    }
    if (!imax){ //se auti tin periptwsi de vrike leksi, opote to for teliwse me to imax 0.
                //synexizei o elegxos gia pio mikri katheto mexri na vrei.
      notfound = 1;
      crossingword--;
    }
  }while (notfound && (crossingword >= -1));
  if (crossingword < -1)
    return 2;
  i = imax;
  j = jmax;
  while (crossword[i][--j].letter != '\0'); //den yparxei periptwsi na vgei eksw apo ta oria dioti to staurolekso einai perikleismeno se midenika
  j++; //o vrogxos kai to j++; fernoun tis syntetagmenes sto prwto gramma tis leksis i opoia tha mpei.
  if (pointers[wordnum] != NULL){
    if ((pointers[wordnum] = pointers[wordnum]->next) == NULL){
      freemem_list(wordsarray[wordnum]); //an den yparxoun alles diathesimes lekseis, tote eleutherwnetai i mnimi kai epistrefetai 0.
      wordsarray[wordnum] = NULL; //o pointers[wordnum] idi deixnei NULL.
      return 0;
    }
    imax = 0; //edw to imax xrisimopoieitai apla san metritis.
    while (crossword[i][j].letter = pointers[wordnum]->word[imax++])//to j den auksanetai edw dioti prepei na mpei kai i pliroforia pio katw sto idio keli.
      crossword[i][j++].orderhori = wordnum;          //to j auksanetai tin idia stigmi gia na proxwrisei o pio panw elegxos
    return 1;
  }
  imax = 0; //ta imax kai j max xrisimopoiountai apla san metrites.
  jmax = j;
  while(buffer[imax++] = crossword[i][jmax++].letter); //Ston buffer grafetai kai to '\0'.
  notfound = 0;
  checkfit(thetree[strlen(buffer) - 2], buffer, &notfound); //ginetai elegxos an sto leksiko perilamvanetai estw kai mia leksi pou sximatizetai apo ton buffer
  if (!notfound)
    return 0;
  buildlist(buffer, &wordsarray[wordnum]);
  //preparesort(wordsarray[wordnum]);
  //mergesort(&sortdata);
  pointers[wordnum] = wordsarray[wordnum];// = sortdata.start;
  imax = 0;
  while (crossword[i][j].letter = pointers[wordnum]->word[imax++]) //eggrafi tis leksis sto staurolekso.
    crossword[i][j++].orderhori = wordnum;
  return 1;
}
