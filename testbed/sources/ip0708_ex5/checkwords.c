#include "functions.h"
#include "structs_functions_ext.h"

int checkwords(int cross_size){ //synartisi pou kaleitai an yparxei to -check argument.
  char buffer[1024];            //geniki leitourgia: prospelazei to staurolekso vazontas tis lekseis mia mia orizontia
  int i = 1, j = 1, bufcount = 0, check = 0, count = 0; //molis symplirwthoun oi orizonties, pairnei tis kathetes mia mia
  while (!crossword[i][j].wordlenhori){ //kai elegxei an tairiazoun. Typwnei ta katallila minimata lathous an to staurolekso de symplirwthei swsta.
    j++;
    if (j > cross_size){
      j = 1;
      i++;
    }
    if (i > cross_size)
      break;
  }
  while (i <= cross_size){
    if (fgets(buffer, 1024, stdin) == NULL){
      printf("Not enough words\n");
      return 0;
    }
    *strrchr(buffer, '\n') = '\0'; //de xreiazetai elegxos giauto dioti to programma panta eksagei '\n' meta apo kathe leksi
    check = 0;
    checkfit(thetree[strlen(buffer) - 2], buffer, &check);
    if (!check){
      printf("Word \"%s\" does not exist in the dictionary, quitting\n", buffer);
      return 0;
    }
    count = 0;
    if (crossword[i][j].wordlenhori == strlen(buffer)){
      while (buffer[count] != '\0')
        crossword[i][j++].letter = buffer[count++];
      while (crossword[i][j].letter == '\0'){
        j++;
        if (j > cross_size){
          j = 1;
          i++;
        }
        if (i > cross_size){
          break;
        }
      }  
    }
    else{
      printf("Word \"%s\" is not equal in length to the size of word expected (%d) for this slot\n", buffer, crossword[i][j].wordlenhori);
      return 0;
    }
  }
  i = j = 1;
  while (!crossword[i][j].wordlenvert){
    i++;
    if (i > cross_size){
      i = 1;
      j++;
    }
    if (j > cross_size)
      break;
  }
  while (j <= cross_size){
    if (fgets(buffer, 1024, stdin) == NULL){
      printf("Not enough words\n");
      return 0;
    }
    *strrchr(buffer, '\n') = '\0'; //de xreiazetai elegxos giauto dioti to programma panta eksagei '\n' meta apo kathe leksi
    check = 0;
    checkfit(thetree[strlen(buffer) - 2], buffer, &check);
    if (!check){
      printf("Word \"%s\" does not exist in the dictionary. Quitting...\n", buffer);
      return 0;
    }
    count = 0;
    if (crossword[i][j].wordlenvert == strlen(buffer)){
      while (buffer[count] != '\0'){
        if (crossword[i][j].letter == buffer[count]){
          i++;
          count++;
        }
        else{
          printf("Vertical word formed by horizontal words input before does not match given vertical word (\"%s\"). Quitting...\n", buffer);
          return 0;
        }
      }
      while (crossword[i][j].letter == '\0'){
        i++;
        if (i > cross_size){
          i = 1;
          j++;
        }
        if (j > cross_size)
          break;
      }  
    }
    else{
      printf("Word \"%s\" is not equal in length to the size of word expected (%d) for this slot\n", buffer, crossword[i][j].wordlenvert);
      return 0;
    }
  }
  if (getchar() != EOF){
    printf("More words than needed\n");
    return 0;
  }
  return 1;
}

