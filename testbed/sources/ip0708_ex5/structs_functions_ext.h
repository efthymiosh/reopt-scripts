//#ifndef EXTERNALSTRUCTSANDFUNCTIONS
//#define EXTERNALSTRUCTSANDFUNCTION

struct list{
  char* word;
  unsigned int weight; //to "varos" tis leksis analoga me tis syxnotites emfanisis twn grammatwn sto leksiko.
  struct list* next;
}**wordsarray, **pointers;

typedef struct list list;

struct treenode{
  char letter;
  struct treenode* left;
  struct treenode* right;
  struct treenode* down;
} **thetree;

typedef struct treenode tree;

struct crossword_cell{
  int orderhori;
  int ordervert;
  int wordlenhori;
  int wordlenvert;
  char letter;
} **crossword;

typedef struct crossword_cell cell;

struct list_headandtail {
    list* start;             //auta den xrisimopoiountai katholou sto programma.
    list* end;
    int size;
} sortdata;

typedef struct list_headandtail mergedata;

void buildlist(char*, list**);
tree* insertword(char*, tree*);
void preparesort(list*); //den xrisimopoieitai.
void freemem_list(list*);
void mergesort(mergedata*); //den xrisimopoieitai.
void checkfit(tree*, char*, int*);
void freetree(tree*);
//#endif
