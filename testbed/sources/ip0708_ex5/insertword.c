#include "functions.h"
#include "structs_functions_ext.h"

tree* insertword(char* word, tree* treeptr){
  if (treeptr == NULL){ //simatodotei oti exoume ftasei se telos tou dentrou
    treeptr = malloc(sizeof(tree));
    treeptr->letter = *word;
    treeptr->left = NULL;
    treeptr->right = NULL;
    treeptr->down = NULL; 
  }
  if (treeptr->letter > *word) //an to gramma einai pio prin apo to gramma tou deikti
    treeptr->left = insertword(word, treeptr->left); //i synartisi epanalamvanetai anadromika gia ton deikti aristera
  else if (treeptr->letter == *word){
    if (*word != 0) //an o deixtis den exei ftasei sto telos, epanalamvanetai i synartisi gia to epomeno gramma, ston deikti ->down (idia leksi)
      treeptr->down = insertword(++word, treeptr->down);
  } 
  else
    treeptr->right = insertword(word, treeptr->right);
  return treeptr;
}

