#include "functions.h"
#include "structs_functions_ext.h"

void freetree(tree* pointer){
  if (pointer == NULL)
    return;
  freetree(pointer->right);
  freetree(pointer->down);
  freetree(pointer->left);
  free(pointer);
  return;
}
