#include "functions.h"
#include "structs_functions_ext.h"

main(int argc,char **argv){
  int i, j, check, wordsum, cross_size;
  char *dict = "Words.txt";
  check = checkarguments(argc, argv);
  if (check == -1)
    return EXIT_FAILURE;
  for (i = 1; i < argc; i++) //o elegxos gia dict ginetai ksexwrista gia na aplopoiithei to switch.
    if (!strcmp(argv[i], "-dict"))
      break;
  if (i < argc){
    if (i == (argc - 1)){
      printf("No dictionary file specified. Quitting..\n");
      return EXIT_FAILURE;
    }
    dict = argv[i + 1];
  }
  if (!buildcells(argv[1], &cross_size, &wordsum)){
    printf("Error opening the crossword file - %s. Quitting..\n", argv[1]);
    return EXIT_FAILURE;
  }
  if (!buildtree(dict, cross_size)){
    printf("The program was unable to build the dictionary. Quitting..\n");
    return EXIT_FAILURE;
  }

  switch (check){
    case  0:  buildcrossword(cross_size, wordsum);
              printcross(cross_size);

              freeall(cross_size, wordsum);
              return EXIT_SUCCESS;
 

    case  1:  checkwords(cross_size);

              for (i = 0; i < cross_size + 2; i++)
                free(crossword[i]);
              free(crossword);
              for (i = 0; i < (cross_size - 1); i++)
                freetree(thetree[i]);
              free(thetree);
              return EXIT_SUCCESS;


    case 10:  buildcrossword(cross_size, wordsum);
              drawcross(cross_size);

              freeall(cross_size, wordsum);
              return EXIT_SUCCESS;


    case 11:  if (checkwords(cross_size))
                drawcross(cross_size);

              for (i = 0; i < cross_size + 2; i++)
                free(crossword[i]);
              free(crossword);
              for (i = 0; i < cross_size - 1; i++)
                freetree(thetree[i]);
              free(thetree);
              return EXIT_SUCCESS;
  }
}
