#include "functions.h"
#include "structs_functions_ext.h"

void preparesort(list* pointer){ //poly apli synartisi pou vriskei to "varos" tis leksis analoga me
  int buffer = 0, i = 0;         //to synolo twn syxnotitwn emfanisis twn grammatwn sto leksiko, i kalytera, sto triadiko dentro.
  sortdata.start = pointer;     //Apothikeuei to varos stin ekastote domi, kai tautoxrona dinei ta aparaitita stoixeia stin synartisi
  while (1){                     //pou tha akolouthisei, tin mergesort.
    for (i = 0; pointer->word[i]; i++)
      buffer += freqtable[pointer->word[i] - 'a'];
    pointer->weight = buffer;
    i++;
    buffer = 0;
    if (pointer->next == NULL){
      sortdata.end = pointer;
      break;
    }
    pointer = pointer->next;
  }
  sortdata.size = i;
  return;
}
