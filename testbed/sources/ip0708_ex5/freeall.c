#include "functions.h"
#include "structs_functions_ext.h"

int freeall(int cross_size, int wordsum){
  int i;
  for (i = 0; i < wordsum; i++)
    freemem_list(wordsarray[i]);
  free(wordsarray); //o logos pou i wordsarray einai orismeni san ekswteriki.
  free(pointers);
  for (i = 0; i < cross_size + 2; i++)
    free(crossword[i]);
  free(crossword);
  for (i = 0; i < cross_size - 1; i++) //mexri cross_size -1. thesi 0: lekseis 2grammatwn, thesi cross_size - 2: lekseis cross_size grammatwn
    freetree(thetree[i]);
  free(thetree);
}
