	.file	"ip1314_ex1.c"
# GNU C (Debian 4.7.2-5) version 4.7.2 (x86_64-linux-gnu)
#	compiled by GNU C version 4.7.2, GMP version 5.0.5, MPFR version 3.1.0-p10, MPC version 0.9
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -imultiarch x86_64-linux-gnu ip1314_ex1.c -mtune=generic
# -march=x86-64 -auxbase-strip ip1314_ex1_O3.s -O3 -fverbose-asm
# options enabled:  -fasynchronous-unwind-tables -fauto-inc-dec
# -fbranch-count-reg -fcaller-saves -fcombine-stack-adjustments -fcommon
# -fcompare-elim -fcprop-registers -fcrossjumping -fcse-follow-jumps
# -fdebug-types-section -fdefer-pop -fdelete-null-pointer-checks
# -fdevirtualize -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -fexpensive-optimizations
# -fforward-propagate -ffunction-cse -fgcse -fgcse-after-reload -fgcse-lm
# -fgnu-runtime -fguess-branch-probability -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions -finline-functions-called-once
# -finline-small-functions -fipa-cp -fipa-cp-clone -fipa-profile
# -fipa-pure-const -fipa-reference -fipa-sra -fira-share-save-slots
# -fira-share-spill-slots -fivopts -fkeep-static-consts
# -fleading-underscore -fmath-errno -fmerge-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-register-move
# -foptimize-sibling-calls -foptimize-strlen -fpartial-inlining -fpeephole
# -fpeephole2 -fpredictive-commoning -fprefetch-loop-arrays -free
# -freg-struct-return -fregmove -freorder-blocks -freorder-functions
# -frerun-cse-after-loop -fsched-critical-path-heuristic
# -fsched-dep-count-heuristic -fsched-group-heuristic -fsched-interblock
# -fsched-last-insn-heuristic -fsched-rank-heuristic -fsched-spec
# -fsched-spec-insn-heuristic -fsched-stalled-insns-dep -fschedule-insns2
# -fshow-column -fshrink-wrap -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fthread-jumps -ftoplevel-reorder
# -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce -ftree-ccp
# -ftree-ch -ftree-copy-prop -ftree-copyrename -ftree-cselim -ftree-dce
# -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-distribute-patterns -ftree-loop-if-convert -ftree-loop-im
# -ftree-loop-ivcanon -ftree-loop-optimize -ftree-parallelize-loops=
# -ftree-phiprop -ftree-pre -ftree-pta -ftree-reassoc -ftree-scev-cprop
# -ftree-sink -ftree-slp-vectorize -ftree-sra -ftree-switch-conversion
# -ftree-tail-merge -ftree-ter -ftree-vect-loop-version -ftree-vectorize
# -ftree-vrp -funit-at-a-time -funswitch-loops -funwind-tables
# -fvect-cost-model -fverbose-asm -fzero-initialized-in-bss
# -m128bit-long-double -m64 -m80387 -maccumulate-outgoing-args
# -malign-stringops -mfancy-math-387 -mfp-ret-in-387 -mglibc -mieee-fp
# -mmmx -mno-sse4 -mpush-args -mred-zone -msse -msse2 -mtls-direct-seg-refs

	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Current time is: %d\n\n"
.LC1:
	.string	"%10d is a Smith Number\n"
.LC4:
	.string	"Found %.2f%% Smith Numbers\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB21:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	xorl	%edi, %edi	#
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movl	$1717986919, %r12d	#, tmp684
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	movl	$100000, %ebp	#, ivtmp.31
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
	call	time	#
	movl	%eax, %edi	# D.2941,
	movq	%rax, %rbx	#, D.2941
	call	srand	#
	movq	stderr(%rip), %rdi	# stderr,
	movl	%ebx, %edx	# D.2941,
	movl	$.LC0, %esi	#,
	xorl	%eax, %eax	#
	xorl	%ebx, %ebx	# smithCount
	call	fprintf	#
.L47:
	call	rand	#
	movl	%eax, %r13d	#, D.2944
	call	rand	#
	movl	%r13d, %ecx	# D.2944, tmp369
	sarl	$31, %ecx	#, tmp369
	shrl	$17, %ecx	#, tmp369
	leal	0(%r13,%rcx), %edx	#, tmp372
	andl	$32767, %edx	#, tmp372
	subl	%ecx, %edx	# tmp369, tmp372
	movl	%eax, %ecx	# D.2947, tmp376
	sarl	$31, %ecx	#, tmp376
	addl	$1, %edx	#, tmp381
	shrl	$17, %ecx	#, tmp376
	addl	%ecx, %eax	# tmp376, tmp379
	andl	$32767, %eax	#, tmp379
	subl	%ecx, %eax	# tmp376, tmp379
	addl	$1, %eax	#, tmp380
	imull	%eax, %edx	# tmp380, tmp381
	leal	1(%rdx), %r9d	#, rndTemp2
	testl	%r9d, %r9d	# rndTemp2
	jle	.L48	#,
	movl	%r9d, %ecx	# rndTemp2, rndTemp
	xorl	%r8d, %r8d	# digitSum
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%ecx, %eax	# rndTemp,
	imull	%r12d	# tmp684
	movl	%ecx, %eax	# rndTemp, tmp386
	sarl	$31, %eax	#, tmp386
	sarl	$2, %edx	#, tmp382
	subl	%eax, %edx	# tmp386, tmp382
	leal	(%rdx,%rdx,4), %eax	#, tmp389
	addl	%eax, %eax	# tmp390
	subl	%eax, %ecx	# tmp390, tmp391
	addl	%ecx, %r8d	# tmp391, digitSum
	testl	%edx, %edx	# rndTemp
	movl	%edx, %ecx	# tmp382, rndTemp
	jne	.L3	#,
.L2:
	xorl	%ecx, %ecx	# primeSum
	testb	$1, %r9b	#, rndTemp2
	movl	%r9d, %esi	# rndTemp2, rndTemp2
	jne	.L4	#,
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%esi, %eax	# rndTemp2, tmp399
	addl	$2, %ecx	#, primeSum
	shrl	$31, %eax	#, tmp399
	addl	%eax, %esi	# tmp399, rndTemp2
	sarl	%esi	# rndTemp2
	testb	$1, %sil	#, rndTemp2
	je	.L5	#,
.L4:
	movl	%esi, %eax	# rndTemp2,
	movl	$1431655766, %edx	#, tmp405
	imull	%edx	# tmp405
	movl	%esi, %eax	# rndTemp2, tmp406
	sarl	$31, %eax	#, tmp406
	subl	%eax, %edx	# tmp406, tmp403
	leal	(%rdx,%rdx,2), %eax	#, tmp409
	cmpl	%eax, %esi	# tmp409, rndTemp2
	jne	.L6	#,
	movl	$1431655766, %edi	#, tmp682
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %esi	#, tmp414
	addl	$3, %ecx	#, primeSum
	imull	%edi	# tmp682
	subl	%esi, %edx	# tmp414, tmp412
	movl	%edx, %eax	# rndTemp2,
	movl	%edx, %esi	# tmp412, rndTemp2
	imull	%edi	# tmp682
	movl	%esi, %eax	# rndTemp2, tmp418
	sarl	$31, %eax	#, tmp418
	subl	%eax, %edx	# tmp418, tmp415
	leal	(%rdx,%rdx,2), %eax	#, tmp421
	cmpl	%eax, %esi	# tmp421, rndTemp2
	je	.L103	#,
	jmp	.L6	#
	.p2align 4,,10
	.p2align 3
.L149:
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %esi	#, tmp436
	addl	$5, %ecx	#, primeSum
	imull	%r12d	# tmp684
	sarl	%edx	# tmp435
	subl	%esi, %edx	# tmp436, tmp435
	movl	%edx, %esi	# tmp435, rndTemp2
.L6:
	movl	%esi, %eax	# rndTemp2,
	imull	%r12d	# tmp684
	movl	%esi, %eax	# rndTemp2, tmp427
	sarl	$31, %eax	#, tmp427
	sarl	%edx	# tmp423
	subl	%eax, %edx	# tmp427, tmp423
	leal	(%rdx,%rdx,4), %eax	#, tmp430
	cmpl	%eax, %esi	# tmp430, rndTemp2
	je	.L149	#,
	cmpl	$48, %esi	#, rndTemp2
	jle	.L11	#,
	movl	$7, %edi	#, i
.L10:
	movl	%esi, %eax	# rndTemp2, tmp637
	movl	%eax, %edx	# tmp465, tmp464
	sarl	$31, %edx	#, tmp464
	idivl	%edi	# i
	testl	%edx, %edx	# tmp464
	jne	.L14	#,
	.p2align 4,,10
	.p2align 3
.L150:
	movl	%edi, %r10d	# i, temp
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp450
	sarl	$31, %eax	#, tmp450
	sarl	$2, %edx	#, tmp446
	subl	%eax, %edx	# tmp450, tmp446
	leal	(%rdx,%rdx,4), %eax	#, tmp453
	addl	%eax, %eax	# tmp454
	subl	%eax, %r10d	# tmp454, tmp455
	addl	%r10d, %ecx	# tmp455, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp446, temp
	jne	.L12	#,
	movl	%esi, %edx	#, tmp462
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp462
	idivl	%edi	# i
	movl	%eax, %edx	# tmp465, tmp464
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp464
	idivl	%edi	# i
	testl	%edx, %edx	# tmp464
	je	.L150	#,
.L14:
	leal	4(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp466
	imull	%r11d, %eax	# temp, tmp466
	cmpl	%esi, %eax	# rndTemp2, tmp466
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp469
	movl	%eax, %edx	# tmp489, tmp488
	sarl	$31, %edx	#, tmp488
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp488
	jne	.L16	#,
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp474
	sarl	$31, %eax	#, tmp474
	sarl	$2, %edx	#, tmp470
	subl	%eax, %edx	# tmp474, tmp470
	leal	(%rdx,%rdx,4), %eax	#, tmp477
	addl	%eax, %eax	# tmp478
	subl	%eax, %r10d	# tmp478, tmp479
	addl	%r10d, %ecx	# tmp479, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp470, temp
	jg	.L17	#,
	movl	%esi, %edx	#, tmp486
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp486
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp489, tmp488
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp488
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp488
	je	.L151	#,
.L16:
	leal	6(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp490
	imull	%r11d, %eax	# temp, tmp490
	cmpl	%esi, %eax	# rndTemp2, tmp490
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp493
	movl	%eax, %edx	# tmp513, tmp512
	sarl	$31, %edx	#, tmp512
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp512
	jne	.L20	#,
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp498
	sarl	$31, %eax	#, tmp498
	sarl	$2, %edx	#, tmp494
	subl	%eax, %edx	# tmp498, tmp494
	leal	(%rdx,%rdx,4), %eax	#, tmp501
	addl	%eax, %eax	# tmp502
	subl	%eax, %r10d	# tmp502, tmp503
	addl	%r10d, %ecx	# tmp503, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp494, temp
	jg	.L21	#,
	movl	%esi, %edx	#, tmp510
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp510
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp513, tmp512
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp512
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp512
	je	.L152	#,
.L20:
	leal	10(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp514
	imull	%r11d, %eax	# temp, tmp514
	cmpl	%esi, %eax	# rndTemp2, tmp514
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp517
	movl	%eax, %edx	# tmp537, tmp536
	sarl	$31, %edx	#, tmp536
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp536
	jne	.L24	#,
	.p2align 4,,10
	.p2align 3
.L153:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp522
	sarl	$31, %eax	#, tmp522
	sarl	$2, %edx	#, tmp518
	subl	%eax, %edx	# tmp522, tmp518
	leal	(%rdx,%rdx,4), %eax	#, tmp525
	addl	%eax, %eax	# tmp526
	subl	%eax, %r10d	# tmp526, tmp527
	addl	%r10d, %ecx	# tmp527, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp518, temp
	jg	.L25	#,
	movl	%esi, %edx	#, tmp534
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp534
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp537, tmp536
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp536
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp536
	je	.L153	#,
.L24:
	leal	12(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp538
	imull	%r11d, %eax	# temp, tmp538
	cmpl	%esi, %eax	# rndTemp2, tmp538
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp541
	movl	%eax, %edx	# tmp561, tmp560
	sarl	$31, %edx	#, tmp560
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp560
	jne	.L28	#,
	.p2align 4,,10
	.p2align 3
.L154:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp546
	sarl	$31, %eax	#, tmp546
	sarl	$2, %edx	#, tmp542
	subl	%eax, %edx	# tmp546, tmp542
	leal	(%rdx,%rdx,4), %eax	#, tmp549
	addl	%eax, %eax	# tmp550
	subl	%eax, %r10d	# tmp550, tmp551
	addl	%r10d, %ecx	# tmp551, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp542, temp
	jg	.L29	#,
	movl	%esi, %edx	#, tmp558
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp558
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp561, tmp560
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp560
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp560
	je	.L154	#,
.L28:
	leal	16(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp562
	imull	%r11d, %eax	# temp, tmp562
	cmpl	%esi, %eax	# rndTemp2, tmp562
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp565
	movl	%eax, %edx	# tmp585, tmp584
	sarl	$31, %edx	#, tmp584
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp584
	jne	.L32	#,
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp570
	sarl	$31, %eax	#, tmp570
	sarl	$2, %edx	#, tmp566
	subl	%eax, %edx	# tmp570, tmp566
	leal	(%rdx,%rdx,4), %eax	#, tmp573
	addl	%eax, %eax	# tmp574
	subl	%eax, %r10d	# tmp574, tmp575
	addl	%r10d, %ecx	# tmp575, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp566, temp
	jg	.L33	#,
	movl	%esi, %edx	#, tmp582
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp582
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp585, tmp584
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp584
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp584
	je	.L155	#,
.L32:
	leal	22(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp586
	imull	%r11d, %eax	# temp, tmp586
	cmpl	%esi, %eax	# rndTemp2, tmp586
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp589
	movl	%eax, %edx	# tmp609, tmp608
	sarl	$31, %edx	#, tmp608
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp608
	jne	.L36	#,
	.p2align 4,,10
	.p2align 3
.L156:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp594
	sarl	$31, %eax	#, tmp594
	sarl	$2, %edx	#, tmp590
	subl	%eax, %edx	# tmp594, tmp590
	leal	(%rdx,%rdx,4), %eax	#, tmp597
	addl	%eax, %eax	# tmp598
	subl	%eax, %r10d	# tmp598, tmp599
	addl	%r10d, %ecx	# tmp599, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp590, temp
	jg	.L37	#,
	movl	%esi, %edx	#, tmp606
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp606
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp609, tmp608
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp608
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp608
	je	.L156	#,
.L36:
	leal	24(%rdi), %r11d	#, temp
	movl	%r11d, %eax	# temp, tmp610
	imull	%r11d, %eax	# temp, tmp610
	cmpl	%esi, %eax	# rndTemp2, tmp610
	jg	.L11	#,
	movl	%esi, %eax	# rndTemp2, tmp613
	movl	%eax, %edx	# tmp633, tmp632
	sarl	$31, %edx	#, tmp632
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp632
	jne	.L40	#,
	.p2align 4,,10
	.p2align 3
.L157:
	movl	%r11d, %r10d	# temp, temp
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%r10d, %eax	# temp,
	imull	%r12d	# tmp684
	movl	%r10d, %eax	# temp, tmp618
	sarl	$31, %eax	#, tmp618
	sarl	$2, %edx	#, tmp614
	subl	%eax, %edx	# tmp618, tmp614
	leal	(%rdx,%rdx,4), %eax	#, tmp621
	addl	%eax, %eax	# tmp622
	subl	%eax, %r10d	# tmp622, tmp623
	addl	%r10d, %ecx	# tmp623, primeSum
	testl	%edx, %edx	# temp
	movl	%edx, %r10d	# tmp614, temp
	jg	.L41	#,
	movl	%esi, %edx	#, tmp630
	movl	%esi, %eax	# rndTemp2,
	sarl	$31, %edx	#, tmp630
	idivl	%r11d	# temp
	movl	%eax, %edx	# tmp633, tmp632
	movl	%eax, %esi	#, rndTemp2
	sarl	$31, %edx	#, tmp632
	idivl	%r11d	# temp
	testl	%edx, %edx	# tmp632
	je	.L157	#,
.L40:
	addl	$30, %edi	#, i
	movl	%edi, %eax	# i, tmp634
	imull	%edi, %eax	# i, tmp634
	cmpl	%esi, %eax	# rndTemp2, tmp634
	jle	.L10	#,
.L11:
	cmpl	%r9d, %esi	# rndTemp2, rndTemp2
	je	.L44	#,
	cmpl	$1, %esi	#, rndTemp2
	je	.L44	#,
	testl	%esi, %esi	# rndTemp2
	.p2align 4,,3
	jle	.L44	#,
.L93:
	movl	%esi, %eax	# rndTemp2,
	imull	%r12d	# tmp684
	movl	%esi, %eax	# rndTemp2, tmp646
	sarl	$31, %eax	#, tmp646
	sarl	$2, %edx	#, tmp642
	subl	%eax, %edx	# tmp646, tmp642
	leal	(%rdx,%rdx,4), %eax	#, tmp649
	addl	%eax, %eax	# tmp650
	subl	%eax, %esi	# tmp650, tmp651
	addl	%esi, %ecx	# tmp651, primeSum
	testl	%edx, %edx	# rndTemp2
	movl	%edx, %esi	# tmp642, rndTemp2
	jne	.L93	#,
.L44:
	cmpl	%ecx, %r8d	# primeSum, digitSum
	je	.L158	#,
.L46:
	subl	$1, %ebp	#, ivtmp.31
	jne	.L47	#,
	cvtsi2sd	%ebx, %xmm0	# smithCount, tmp660
	movq	stderr(%rip), %rdi	# stderr,
	movl	$.LC4, %esi	#,
	movl	$1, %eax	#,
	mulsd	.LC2(%rip), %xmm0	#, tmp660
	divsd	.LC3(%rip), %xmm0	#, tmp660
	call	fprintf	#
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	xorl	%eax, %eax	#
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	stderr(%rip), %rdi	# stderr,
	movl	%r9d, %edx	# rndTemp2,
	movl	$.LC1, %esi	#,
	xorl	%eax, %eax	#
	addl	$1, %ebx	#, smithCount
	call	fprintf	#
	jmp	.L46	#
.L48:
	xorl	%r8d, %r8d	# digitSum
	jmp	.L2	#
	.cfi_endproc
.LFE21:
	.size	main, .-main
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1079574528
	.align 8
.LC3:
	.long	0
	.long	1090021888
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
