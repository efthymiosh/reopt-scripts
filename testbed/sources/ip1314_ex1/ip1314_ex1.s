	.file	"ip1314_ex1.c"
	.section	.rodata
.LC0:
	.string	"%10d is a Smith Number\n"
.LC3:
	.string	"Found %.2f%% Smith Numbers\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
#APP
# 8 "ip1314_ex1.c" 1
	jmp STEP1
STEP1: nopl 0x13371337
	
# 0 "" 2
#NO_APP
	movl	$3, -24(%rbp)
	movl	$0, -20(%rbp)
	jmp	.L2
.L57:
	movl	-24(%rbp), %edx
	movl	%edx, %eax
	sall	$3, %eax
	subl	%edx, %eax
	leal	1(%rax), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$17, %eax
	addl	%eax, %edx
	andl	$32767, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	leal	1(%rax), %ecx
	movl	-24(%rbp), %edx
	movl	%edx, %eax
	sall	$3, %eax
	subl	%edx, %eax
	leal	1(%rax), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$17, %eax
	addl	%eax, %edx
	andl	$32767, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	addl	$1, %eax
	imull	%ecx, %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, -28(%rbp)
	movl	-28(%rbp), %eax
	movl	%eax, -4(%rbp)
	movl	$0, -36(%rbp)
	movl	$0, -32(%rbp)
	jmp	.L3
.L4:
	movl	-28(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -36(%rbp)
	movl	-28(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -28(%rbp)
.L3:
	cmpl	$0, -28(%rbp)
	jg	.L4
	jmp	.L5
.L6:
	addl	$2, -32(%rbp)
	movl	-12(%rbp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -12(%rbp)
.L5:
	movl	-12(%rbp), %eax
	andl	$1, %eax
	testl	%eax, %eax
	je	.L6
	jmp	.L7
.L8:
	addl	$3, -32(%rbp)
	movl	-12(%rbp), %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -12(%rbp)
.L7:
	movl	-12(%rbp), %ecx
	movl	$1431655766, %edx
	movl	%ecx, %eax
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	addl	%edx, %edx
	addl	%eax, %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	testl	%eax, %eax
	je	.L8
	jmp	.L9
.L10:
	addl	$5, -32(%rbp)
	movl	-12(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -12(%rbp)
.L9:
	movl	-12(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, %edx
	sall	$2, %edx
	addl	%eax, %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	testl	%eax, %eax
	je	.L10
	movl	$7, -16(%rbp)
	jmp	.L11
.L15:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L13
.L14:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L13:
	cmpl	$0, -8(%rbp)
	jg	.L14
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L12:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L15
	addl	$4, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L59
	jmp	.L18
.L21:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L19
.L20:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L19:
	cmpl	$0, -8(%rbp)
	jg	.L20
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L18:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L21
	addl	$2, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L60
	jmp	.L23
.L26:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L24
.L25:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L24:
	cmpl	$0, -8(%rbp)
	jg	.L25
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L23:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L26
	addl	$4, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L61
	jmp	.L28
.L31:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L29
.L30:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L29:
	cmpl	$0, -8(%rbp)
	jg	.L30
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L28:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L31
	addl	$2, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L62
	jmp	.L33
.L36:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L34
.L35:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L34:
	cmpl	$0, -8(%rbp)
	jg	.L35
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L33:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L36
	addl	$4, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L63
	jmp	.L38
.L41:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L39
.L40:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L39:
	cmpl	$0, -8(%rbp)
	jg	.L40
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L38:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L41
	addl	$6, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L64
	jmp	.L43
.L46:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L44
.L45:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L44:
	cmpl	$0, -8(%rbp)
	jg	.L45
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L43:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L46
	addl	$2, -16(%rbp)
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jg	.L65
	jmp	.L48
.L51:
	movl	-16(%rbp), %eax
	movl	%eax, -8(%rbp)
	jmp	.L49
.L50:
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	addl	%edx, -32(%rbp)
	movl	-8(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -8(%rbp)
.L49:
	cmpl	$0, -8(%rbp)
	jg	.L50
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%eax, -12(%rbp)
.L48:
	movl	-12(%rbp), %eax
	cltd
	idivl	-16(%rbp)
	movl	%edx, %eax
	testl	%eax, %eax
	je	.L51
	addl	$6, -16(%rbp)
.L11:
	movl	-16(%rbp), %eax
	imull	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jle	.L12
	jmp	.L17
.L59:
	nop
	jmp	.L17
.L60:
	nop
	jmp	.L17
.L61:
	nop
	jmp	.L17
.L62:
	nop
	jmp	.L17
.L63:
	nop
	jmp	.L17
.L64:
	nop
	jmp	.L17
.L65:
	nop
.L17:
	cmpl	$1, -12(%rbp)
	je	.L53
	movl	-12(%rbp), %eax
	cmpl	-4(%rbp), %eax
	je	.L53
	jmp	.L54
.L55:
	movl	-12(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	addl	%eax, -32(%rbp)
	movl	-12(%rbp), %ecx
	movl	$1717986919, %edx
	movl	%ecx, %eax
	imull	%edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	movl	%eax, -12(%rbp)
.L54:
	cmpl	$0, -12(%rbp)
	jg	.L55
.L53:
	movl	-36(%rbp), %eax
	cmpl	-32(%rbp), %eax
	jne	.L56
	movq	stderr(%rip), %rax
	movl	-4(%rbp), %edx
	movl	$.LC0, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	addl	$1, -20(%rbp)
.L56:
	subl	$1, -24(%rbp)
.L2:
	cmpl	$0, -24(%rbp)
	jg	.L57
	pxor	%xmm0, %xmm0
	cvtsi2sd	-20(%rbp), %xmm0
	movsd	.LC1(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	.LC2(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movq	stderr(%rip), %rax
	movl	$.LC3, %esi
	movq	%rax, %rdi
	movl	$1, %eax
	call	fprintf
#APP
# 144 "ip1314_ex1.c" 1
	jmp STEP2
STEP2: nopl 0x13371337
	
# 0 "" 2
#NO_APP
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC1:
	.long	0
	.long	1079574528
	.align 8
.LC2:
	.long	0
	.long	1074266112
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
