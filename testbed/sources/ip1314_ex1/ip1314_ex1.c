#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COMPUTATIONS 3

int main(void){
	asm volatile (
		"jmp STEP1\n"
		"STEP1: nopl 0x13371337\n\t"
	);
    int digitSum, primeSum, rndTemp, rndInit, number, smithCount, tim;
    int i, rndTemp2, temp;
    //tim = time(NULL);
    //srand(tim);
    number = COMPUTATIONS;
    smithCount = 0;
    //fprintf(stderr, "Current time is: %d\n\n", tim);
    while (number > 0){
        //rndInit = rndTemp = rndTemp2 = (rand() % 32768 + 1) * (rand() % 32768 + 1) + 1;
        rndInit = rndTemp = rndTemp2 = ((number * 7 + 1) % 32768 + 1)
                                     * ((number * 7 + 1) % 32768 + 1) + 1;
        digitSum = 0;
        primeSum = 0;
        while (rndTemp > 0){
            digitSum += rndTemp % 10;
            rndTemp /= 10;
        }
        while (!(rndTemp2 % 2)){
            primeSum += 2;
            rndTemp2 /= 2;
        }
        while (!(rndTemp2 % 3)){
            primeSum += 3;
            rndTemp2 /= 3;
        }
        while (!(rndTemp2 % 5)){
            primeSum += 5;
            rndTemp2 /= 5;
        }
        i = 7;
        while ((i * i) <= rndTemp2){
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 4;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 2;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 4;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 2;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 4;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 6;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 2;
            if ((i * i) > rndTemp2)
                break;
            while (!(rndTemp2 % i)){
                temp = i;
                while (temp > 0){
                    primeSum += temp % 10;
                    temp /= 10;
                }
                rndTemp2 /= i;
            }
            i += 6;
        }
        if ((rndTemp2 != 1) && (rndTemp2 != rndInit)){
            while(rndTemp2 > 0){
                i = rndTemp2 % 10;
                primeSum += i;
                rndTemp2 /= 10;
            }
        }
        if (digitSum == primeSum){
            fprintf(stderr, "%10d is a Smith Number\n", rndInit);
            ++smithCount;
        }
        --number;
    }
    fprintf(stderr, "Found %.2f%% Smith Numbers\n", (100.0 * smithCount) / COMPUTATIONS);
	asm volatile (
		"jmp STEP2\n"
		"STEP2: nopl 0x13371337\n\t"
	);
    return EXIT_SUCCESS;
}
