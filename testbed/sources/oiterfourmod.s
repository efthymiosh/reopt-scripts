	.file	"oiterfourmod.c"
	.text
	.globl	hanoi
	.type	hanoi, @function
hanoi:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	cmpl	$64, -4(%rbp)
	jne	.L2
	movq	$-1, %rax
	jmp	.L3
.L2:
	cmpl	$63, -4(%rbp)
	jg	.L4
	movl	-4(%rbp), %eax
	movl	$1, %edx
	movl	%eax, %ecx
	salq	%cl, %rdx
	movq	%rdx, %rax
	subq	$1, %rax
	jmp	.L3
.L4:
	movl	$0, %eax
.L3:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	hanoi, .-hanoi
	.globl	iterfour
	.type	iterfour, @function
iterfour:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$14472, %rsp
	.cfi_offset 3, -24
	movl	%edi, -14468(%rbp)
	movq	%rsi, -14480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, -14468(%rbp)
	jne	.L6
	movq	-14480(%rbp), %rax
	movl	$1, (%rax)
	movl	$1, %eax
	jmp	.L17
.L6:
	leaq	-14432(%rbp), %rax
	movq	%rax, -14456(%rbp)
	movq	-14456(%rbp), %rax
	movq	$1, (%rax)
	movl	$2, -14464(%rbp)
	jmp	.L8
.L16:
	movl	-14464(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rdx, %rax
	movl	-14464(%rbp), %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	leaq	-16(%rdx), %rcx
	movq	-14456(%rbp), %rdx
	addq	%rcx, %rdx
	movq	(%rdx), %rdx
	addq	%rdx, %rdx
	addq	$1, %rdx
	movq	%rdx, (%rax)
	movq	-14480(%rbp), %rax
	movl	$1, (%rax)
	movl	-14464(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -14460(%rbp)
.L12:
	movl	-14464(%rbp), %eax
	subl	-14460(%rbp), %eax
	cmpl	$63, %eax
	jle	.L9
	movl	$0, %eax
	jmp	.L17
.L9:
	movl	-14464(%rbp), %eax
	subl	-14460(%rbp), %eax
	movl	%eax, %edi
	call	hanoi
	movq	%rax, %rcx
	movl	-14464(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rax
	cmpq	%rax, %rcx
	ja	.L20
	subl	$1, -14460(%rbp)
	jmp	.L12
.L20:
	nop
	jmp	.L13
.L15:
	movl	-14460(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rax
	leaq	(%rax,%rax), %rbx
	movl	-14464(%rbp), %eax
	subl	-14460(%rbp), %eax
	movl	%eax, %edi
	call	hanoi
	addq	%rbx, %rax
	movq	%rax, -14448(%rbp)
	movl	-14464(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rax
	cmpq	-14448(%rbp), %rax
	jbe	.L14
	movl	-14464(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rax, %rdx
	movq	-14448(%rbp), %rax
	movq	%rax, (%rdx)
	movq	-14480(%rbp), %rax
	movl	-14460(%rbp), %edx
	movl	%edx, (%rax)
.L14:
	addl	$1, -14460(%rbp)
.L13:
	movl	-14460(%rbp), %eax
	cmpl	-14464(%rbp), %eax
	jl	.L15
	addl	$1, -14464(%rbp)
.L8:
	movl	-14464(%rbp), %eax
	cmpl	-14468(%rbp), %eax
	jle	.L16
	movl	-14468(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	-8(%rax), %rdx
	movq	-14456(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rax
	movq	%rax, -14440(%rbp)
	movq	-14440(%rbp), %rax
.L17:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	je	.L18
	call	__stack_chk_fail
.L18:
	addq	$14472, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	iterfour, .-iterfour
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
