//Anadromiki synartisi gia ti lysi tou provlimatos tou hanoi me 4 stylous.
#include "hanoifunctions.h"
unsigned long long rcsfour(int num, int *kay){
  int counter;
  unsigned long long minimum, counted;
  if (num == 1){
    if (kay != NULL)                           //I rcsfour() stamata na kalei ton eauto tis otan ftasei sto 1, opote kai epistrefei 1
      *kay = 1;
    return 1;
  }             //Sti synexeia arxikopoieitai i minimum me h(N,1), pou krata to plithos, kai mpainei arithmos diskwn 1 sti dieuthinsi thesis mnimis
  minimum = 2*rcsfour(1, NULL) + hanoi(num - 1); //Den mas endiaferei o arithmos panw diskwn gia tis anadromikes kliseis tis rcsfour() opote
  if (kay!= NULL)                                //to kay kaleitai me NULL kai se periptwsi NULL den apothikeuetai. O kyrios logos omws paratithetai
    *kay = 1;                                    //pio katw.
  for (counter = 1; counter <= (num - 1); counter++){
    if ((num - counter) >= 64){
      printf("The program had to calculate numbers greater than (2^64 - 1) in a single 8byte variable, which is not possible.\n");
      printf("Please rerun the program using a Natural number equal to or smaller than 64.\n");
      return 0;  //se periptwsi megalyterou apo 64 arithmou, i synartisi epistrefei sfalma an kai gia arithmous
    }            //megalyterous apo 25-26 i synartisi den epistrefei telikes times se logikous xronous etsi ki alliws.
    counted = 2*rcsfour(counter, NULL) + hanoi(num - counter);
    if (minimum > counted){                         //I synartisi kalei ton eauto tis elegxontas an yparxei h(N,K) mikrotero tou apothikeumenou
      minimum = counted;                            //O kyrios logos gia ton opoio ginetai me NULL kai oxi me kay i anadromiki klisi. Oi epomenes
      if (kay!=NULL)                                //kliseis tha grapsoun sto kay, estw kai an den epistrefoun nea elaxisti timi.
        *kay = counter;                             //Sto periexomeno tis thesis mnimis stelnetai to counter, to opoio einai kai o arithmos diskwn
    }                                               //pou prepei na metakinithoun prwtoi wste to plithos na einai to elaxisto
  }
  return minimum;                                   //Apo ti synartisi epistrefetai to elaxisto plithos kinisewn
}
