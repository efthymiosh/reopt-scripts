//Methodos evresis twn elaxistwn plithwn gia plithi mexri 1605.
#include "hanoifunctions.h"
unsigned long long iterfour(int num, int *kay){
    int counter, K;
    unsigned long long numcheck, minimum, *pointer;
    if (num == 1){
        *kay = 1;
        return 1;
    }
    if ((pointer = malloc(num*sizeof(long long))) == NULL){
        printf("Dynamic Memory Allocation Error. Please rerun the program using a smaller number.\n");
        return 0;
    }
    *pointer = 1;
    for (counter = 2; counter <= num; counter++){
        pointer[counter - 1] = 2 * pointer[counter - 2] + 1; //I megalyteri timi pou mporei na parei to h4(N)
        *kay = 1;  //Apodeiksi: http://lists.di.uoa.gr/showpost.php?p=7575&postcount=15
        for (K = counter - 1; ; K--){  //Gia na xrisimopoiisoume to h4(N) <= 2*h4(N-1) + 1, ksekiname anapoda metrwntas apo tin teleutaia timi 
            if ((counter - K) >= 64){   //To programa termatizei, dioti an mono to h3(N-K) ftanei to anwtato orio, tote i teliki metavliti tha yperxeilisei.
                printf("The program had to calculate numbers greater than 2^64 - 1 in a 8 byte variable which is not possible. Please rerun the program using a smaller number\n");
                return 0;
            }
            if (hanoi(counter - K) > pointer[counter - 1]) //Molis to h3(N-K) kseperasei to  ( 2*h4(N-1) + 1 ), tote o vrogxos termatizei, dioti
                break; //gia kathe mikrotero K, to h3(N-K) auksanetai. Opote afou h4(N) = 2*h4(K) + h3(N-K) kai to h4(N) tha exei kseperasei to 2*h4(N-1) + 1.
        }
        for ( ; K < counter; K++){ //Etsi glitwnontai para polloi askopoi elegxoi h(N,K).
            numcheck = 2 * (pointer[K - 1]) + hanoi(counter - K); //An to counter-K den elegxotan, tote to numcheck gia (counter - K) >=64 tha yperxeilize.
            if (pointer[counter - 1] > numcheck){
                pointer[counter - 1] = numcheck;
                *kay = K;
            }
        }
    }
    minimum = pointer[num - 1];
    free(pointer);
    return (minimum);
}
