//Epanaliptiki synartisi gia to provlima twn pyrgwn tou hanoi me 4 stylous
#include "hanoifunctions.h"              //Perilamvanetai kai i apli synartisi 3 stylwn
unsigned long long iterfour(int num, int *kay){
  int counter, K;
  unsigned long long numcheck, minimum, myarray[64];
  if (num == 1){                            //To 1 antimetwpizetai ksexwrista dioti den mporei na mpei mesa sto kyrio skelos tis synartisis
      *kay = 1;                             //To k orizetai 1 dioti see alli periptwsi, to kay den
    return 1;                               //tha eperne timi pote, an kai i timi tis iterfour tha itane swsti.
  }
  *myarray = 1;                                                   //I prwti thesi orizetai eksarxis ws 1
  for (counter = 2; counter <= num; counter++){
    if ((counter - 1) >= 64){
      printf("The program had to calculate numbers greater than (2^64 - 1) in a single 8byte variable, which is not possible.\n");
      printf("Please rerun the program for a Natural number equal to or smaller than 64.\n");
      return 0;
    }
    myarray[counter - 1] = 2*myarray[0] + hanoi(counter - 1);  //prwti timi mpainei to h(N,1)
    *kay = 1;
    for ( K = 2; K < counter; K++){                               //i synartisi apothikeuei arxika to h(N,1) kai meta elegxei apo h(N,2) mexri kai h(N,N-1)
      numcheck = 2 * (myarray[K - 1]) + hanoi(counter - K);       //Se periptwsi pou to h(N,K) einai mikrotero apo tin yparxousa timi
      if (myarray[counter - 1] > numcheck){                       //tote antikathistatai me autin. To plithos kinisewn gia K einai panta gnwsto
        myarray[counter - 1] = numcheck;                          //dioti N>K kai ta proigoumena plithi (apo h4(1) mexri h4(N-1)) einai apothikeumena
        *kay = K;                                                 //ston pinaka
      }
    }
  }
  return (myarray[num - 1]);
}
