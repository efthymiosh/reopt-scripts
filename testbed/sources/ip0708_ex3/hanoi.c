//Poly apli synartisi gia to klasiko provlima twn pyrgwn tou Hanoi me 3 stylous.
unsigned long long hanoi(int num){
  if (num == 64)
    return (-1);  //To -1 pou fainetai edw einai stin ousia to 2^64 - 1, dioti afou i synartisi epistrefei unsigned long long, to apotelesma kanei
  if (num < 64)   //overflow se 2^64 - 1
    return ((1ull << num) - 1); //Apo tin ekfwnisi: h3(N) = 2^N - 1. To 1 (gia metavliti unsigned long long) mpainei stin Niosti(num) dynami tou 2
  return 0;                     //kai meta afaireitai 1.
  /*Gia (num > 64) to apotelesma den mporei na ypologistei, opote oi synartiseis gia 4 stylous typwnoun error - i hanoi den kaleitai katholou
    an kai edw fainetai na epistrefei 0, gia na diatirithei i akeraiotita tis synartisis.
    Gia (num == 64) to apotelesma mporei men na ypologistei, alla dioti to h4(N) einai 2*h4(K) + h3(N-K), an  to N-K einai 2^64 - 1 i metavliti pou
    krataei to h4(N) tha kanei overflow, ara to lathos provlepetai kai se auti tin periptwsi apo tis iterfour kai rcsfour. Gia na 
    diatirithei i akeraiototita tis synartisis, kai pali, epistrefetai to ortho apotelesma kai gia to num == 64*/
}
