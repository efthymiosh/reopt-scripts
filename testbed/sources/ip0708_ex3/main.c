//Main function file for the Four Column Hanoi Towers exercise
#include "hanoifunctions.h"
// main(){
int main(int argc, char *argv[]){
    int number, topdisks, method;
  unsigned long long moves;  //an kai oi kiniseis pote den mporoun na ftasoun tetoia megethi, auto den mporei na apodeixthei. theoritika, mporoun na paroun tetoies times
//   printf("Please insert a valid Natural number:  ");
//   scanf("%d", &number);
  if (argc < 3){
      printf("Please run as: %s [disk amount] [# of method (1 for recursive, 2 for iterative)]", argv[0]);
      return;
  }
  number = atoi(argv[1]);
  while (number <= 0){
    printf ("The Natural Numbers are the integers greater or equal to 1.\nPlease insert a valid Natural number:  ");
    scanf("%d", &number);                             //An to programma parei arithmo mikrotero tou 1, ksanazita apo to xristi na dwsei arithmo
  }
  method = atoi(argv[2]);
  while ((method != 1) && (method != 2)){    //An to programma den pairnei 1 i 2 ws epilogi methodou, ksanazita apo to xristi na dwsei mia epilogi methodou
    printf("You must choose between the recursive or the iterative method. Please type 1 for the recursive or 2 for the iterative method:  ");
    scanf("%u", &method);
  }
  if (method == 1)
    moves = rcsfour(number, &topdisks);   //Anadromiki synartisi Hanoi tessarwn kolwnwn
  else{                                   //An i method den einai 1, tote einai 2, dioti alliws to pio panw while de tha termatize
    moves = iterfour(number, &topdisks);  //Epanaliptiki synartisi Hanoi tessarwn kolwnwn
    if (moves == 0)                       //I iterfour() epistrefei 0 kai typwnei minima lathous otan to N einai megalytero tou 64
      return 1;                           //kai i optimised iterfour() (oiterfour.c) otan yparxei provlima me DMA
  }
  printf ("The minimum number of moves is %llu when removing the %d top disks first\n", moves, topdisks);
}
