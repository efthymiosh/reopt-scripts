

//Poly apli synartisi gia to klasiko provlima twn pyrgwn tou Hanoi me 3 stylous.
unsigned long long hanoi(int num){
  if (num == 64)
    return (-1);  //To -1 pou fainetai edw einai stin ousia to 2^64 - 1, dioti afou i synartisi epistrefei unsigned long long, to apotelesma kanei
  if (num < 64)   //overflow se 2^64 - 1
    return ((1ull << num) - 1); //Apo tin ekfwnisi: h3(N) = 2^N - 1. To 1 (gia metavliti unsigned long long) mpainei stin Niosti(num) dynami tou 2
  return 0;                     //kai meta afaireitai 1.
  /*Gia (num > 64) to apotelesma den mporei na ypologistei, opote oi synartiseis gia 4 stylous typwnoun error - i hanoi den kaleitai katholou
    an kai edw fainetai na epistrefei 0, gia na diatirithei i akeraiotita tis synartisis.
    Gia (num == 64) to apotelesma mporei men na ypologistei, alla dioti to h4(N) einai 2*h4(K) + h3(N-K), an  to N-K einai 2^64 - 1 i metavliti pou
    krataei to h4(N) tha kanei overflow, ara to lathos provlepetai kai se auti tin periptwsi apo tis iterfour kai rcsfour. Gia na 
    diatirithei i akeraiototita tis synartisis, kai pali, epistrefetai to ortho apotelesma kai gia to num == 64*/
}

//Methodos evresis twn elaxistwn plithwn gia plithi mexri 1605.
unsigned long long iterfour(int num, int *kay){
    long long extern_x[1800];
    int counter, K;
    unsigned long long numcheck, minimum, *pointer;
    if (num == 1){
        *kay = 1;
        return 1;
    }
    pointer = extern_x;
    *pointer = 1;
    for (counter = 2; counter <= num; counter++){
        pointer[counter - 1] = 2 * pointer[counter - 2] + 1; //I megalyteri timi pou mporei na parei to h4(N)
        *kay = 1;  //Apodeiksi: http://lists.di.uoa.gr/showpost.php?p=7575&postcount=15
        for (K = counter - 1; ; K--){  //Gia na xrisimopoiisoume to h4(N) <= 2*h4(N-1) + 1, ksekiname anapoda metrwntas apo tin teleutaia timi 
            if ((counter - K) >= 64){   //To programa termatizei, dioti an mono to h3(N-K) ftanei to anwtato orio, tote i teliki metavliti tha yperxeilisei.
                return 0;
            }
            if (hanoi(counter - K) > pointer[counter - 1]) //Molis to h3(N-K) kseperasei to  ( 2*h4(N-1) + 1 ), tote o vrogxos termatizei, dioti
                break; //gia kathe mikrotero K, to h3(N-K) auksanetai. Opote afou h4(N) = 2*h4(K) + h3(N-K) kai to h4(N) tha exei kseperasei to 2*h4(N-1) + 1.
        }
        for ( ; K < counter; K++){ //Etsi glitwnontai para polloi askopoi elegxoi h(N,K).
            numcheck = 2 * (pointer[K - 1]) + hanoi(counter - K); //An to counter-K den elegxotan, tote to numcheck gia (counter - K) >=64 tha yperxeilize.
            if (pointer[counter - 1] > numcheck){
                pointer[counter - 1] = numcheck;
                *kay = K;
            }
        }
    }
    minimum = pointer[num - 1];
    return (minimum);
}
