#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "movl $0, %eax\n\t"
            "movl $1, %ebx\n\t"
            "movl $2, %ecx\n\t"
            "movl $3, %edx\n\t"
            "addl $4, %eax\n\t"
            "addl $8, %ebx\n\t"
            "addl $12, %ecx\n\t"
            "addl $16, %ecx\n\t"
            "addl %eax, %ecx\n\t"
            "addl %ebx, %ecx\n\t"
            "addl %ecx, %edx\n\t"
            "addl %ecx, %eax\n\t"

            "movl $0, %eax\n\t"
            "movl $1, %ebx\n\t"
            "movl $2, %ecx\n\t"
            "movl $3, %edx\n\t"
            "addl $4, %eax\n\t"
            "addl $8, %ebx\n\t"
            "addl $12, %ecx\n\t"
            "addl $16, %ecx\n\t"
            "addl %eax, %ecx\n\t"
            "addl %ebx, %ecx\n\t"
            "addl %ecx, %edx\n\t"
            "addl %ecx, %eax\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
