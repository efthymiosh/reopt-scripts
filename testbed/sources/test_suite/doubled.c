#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "movq $0, %rax\n\t"
            "movq $1, %rbx\n\t"
            "movq $2, %rcx\n\t"
            "movq $3, %rdx\n\t"
            "addq $4, %rax\n\t"
            "addq $8, %rbx\n\t"
            "addq $12, %rcx\n\t"
            "addq $16, %rcx\n\t"
            "addq %rax, %rcx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rcx, %rdx\n\t"
            "addq %rcx, %rax\n\t"

            "movq $0, %rax\n\t"
            "movq $1, %rbx\n\t"
            "movq $2, %rcx\n\t"
            "movq $3, %rdx\n\t"
            "addq $4, %rax\n\t"
            "addq $8, %rbx\n\t"
            "addq $12, %rcx\n\t"
            "addq $16, %rcx\n\t"
            "addq %rax, %rcx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rcx, %rdx\n\t"
            "addq %rcx, %rax\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
