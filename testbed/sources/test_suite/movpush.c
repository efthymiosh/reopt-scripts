#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"
        "movq %rsp, %rax\n\t"
        "movq $5, %rax\n\t"
        "addq   %rax, %r15\n\t"
        //"addq   0x28(%rsp), %r15\n\t"
        "push   %r15\n\t"
        "nopl 0x22222222\n\t"
	);
	return 0;
}
