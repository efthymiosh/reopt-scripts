#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

        "movq    %rsp, %rbp\n\t"
        ".cfi_def_cfa_register 6\n\t"
        "movl    $0, %ebx\n\t"
        "movl    $0, %eax\n\t"
        "jmp .L2\n"
        ".L3:\n\t"
        "addl    $30, %ebx\n\t"
        "addl    $1, %eax\n"
        ".L2:\n\t"
        "cmpl    $4, %eax\n\t"
        "jle .L3\n\t"
        "movl    %ebx, %eax\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
