#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

        "subq $8, %rsp\n\t"
        "movq %rdx, 0(%rsp)\n\t"
        "movq 0(%rsp), %rax\n\t"
        "addq $8, %rax\n\t"
        "addq $9, %rax\n\t"
        "movq %rdx, %rbx\n\t"
        "addq $8, %rbx\n\t"
        "addq $9, %rbx\n\t"
        "addq $8, %rbp\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
