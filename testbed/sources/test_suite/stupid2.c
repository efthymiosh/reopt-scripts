#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

	//code goes here
        "add %rax, -8(%rsp)\n\t"
        "adc -8(%rsp), %rcx\n\t"
        "adc -8(%rsp), %rcx\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
