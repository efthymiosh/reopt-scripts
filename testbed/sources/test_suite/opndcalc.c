#include <stdio.h>

int main(void) {
    asm volatile (
        "nopl 0x11111111\n\t"

        //code goes here
        "lea    (%r8,%rdx,4), %r9\n\t"
        "mov    $0x02, %r8\n\t"
        "mov    $0x03, %rdx\n\t"
        "add    $7, %rax\n\t"

        "nopl 0x22222222\n\t"
    );
    return 0;
}
