#include <stdio.h>

int fn(int a) {
    int b, c;
    return (b = a + a) + (c = a - 7);
}

int main(void) {
	asm volatile ("nopl 0x11111111\n\t");

    int x = fn(3);
    int y = fn(4);
    int z = fn(5);
    
	asm volatile ("nopl 0x22222222\n\t");
	return 0;
}
