#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "addq $4, %rax\n\t"
            "addq $8, %rbx\n\t"
            "addq $12, %rcx\n\t"
            "addq $16, %rcx\n\t"
            "addq %rax, %rcx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rcx, %rdx\n\t"
            "addq %rcx, %rax\n\t"

            "addq $4, %rdx\n\t"
            "addq $8, %rax\n\t"
            "addq $12, %rbx\n\t"
            "addq $16, %rbx\n\t"
            "addq %rdx, %rbx\n\t"
            "addq %rax, %rbx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rbx, %rdx\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
