#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

        "movq    %rsp, %rbp\n\t"
        ".cfi_def_cfa_register 6\n\t"
        "movl    $0, -4(%rbp)\n\t"
        "movl    $0, -8(%rbp)\n\t"
        "jmp .L2\n"
        ".L3:\n\t"
        "addl    $30, -4(%rbp)\n\t"
        "addl    $1, -8(%rbp)\n"
        ".L2:\n\t"
        "cmpl    $4, -8(%rbp)\n\t"
        "jle .L3\n\t"
        "movl    -4(%rbp), %eax\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
