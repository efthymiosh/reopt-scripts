#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "movq $0, %r11\n\t"
            "movq $1, %r12\n\t"
            "movq $2, %r13\n\t"
            "movq $3, %rdx\n\t"
            "addq $4, %r11\n\t"
            "addq $8, %r12\n\t"
            "addq $12, %r13\n\t"
            "addq $16, %r13\n\t"
            "addq %r11, %r13\n\t"
            "addq %r12, %r13\n\t"
            "addq %r13, %rdx\n\t"
            "addq %r13, %r11\n\t"

            "movq $0, %rdx\n\t"
            "movq $1, %rax\n\t"
            "movq $2, %rbx\n\t"
            "movq $3, %rcx\n\t"
            "addq $4, %rdx\n\t"
            "addq $8, %rax\n\t"
            "addq $12, %rbx\n\t"
            "addq $16, %rbx\n\t"
            "addq %rdx, %rbx\n\t"
            "addq %rax, %rbx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rbx, %rdx\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
