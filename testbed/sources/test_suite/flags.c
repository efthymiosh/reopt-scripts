#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "addq $4, %rax\n\t"
            "addq $8, %rbx\n\t"
            "adcq $12, %rcx\n\t"
            "adcq $16, %rcx\n\t"
            "adcq %rax, %rcx\n\t"
            "adcq %rbx, %rcx\n\t"
            "addq %rcx, %rdx\n\t"
            "addq %rcx, %rax\n\t"

            "addq $4, %rdx\n\t"
            "addq $8, %rax\n\t"
            "adcq $12, %rbx\n\t"
            "adcq $16, %rbx\n\t"
            "adcq %rdx, %rbx\n\t"
            "adcq %rax, %rbx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rbx, %rdx\n\t"
            "movq $1, %rdx\n\t"
            "cmpq $8, %rdx\n\t"
            "movq $1, %rdx\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
