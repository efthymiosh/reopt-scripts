#include <stdio.h>

int main(void) {
    asm volatile (
            "nopl 0x11111111\n\t"

            "movl $0, %eax\n\t"
            "movl $1, %ebx\n\t"
            "movl $2, %ecx\n\t"
            "movl $3, %edx\n\t"
            "addw $4, %ax\n\t"
            "addw $8, %bx\n\t"
            "addw $12, %cx\n\t"
            "addw $16, %cx\n\t"
            "addq %rax, %rcx\n\t"
            "addq %rbx, %rcx\n\t"
            "addq %rcx, %rdx\n\t"
            "addq %rcx, %rax\n\t"

            "mov %eax, %ebx\n\t"

            "nopl 0x22222222\n\t"
    );
    return 0;
}
