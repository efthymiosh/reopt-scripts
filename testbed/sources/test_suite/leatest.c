#include <stdio.h>

int main(void) {
    asm volatile (
        "nopl 0x11111111\n\t"

        //code goes here
        "add    $0x04, %rdi\n\t"
        "mov    (%rdi), %ecx\n\t"
        "mov    0x00000308(%rax), %r8\n\t"
        "mov    %ecx, %edx\n\t"
        "lea    (%r8,%rdx,4), %r8\n\t"
        "lea    (%rdx,%rdx,2), %rdx\n\t"
        "lea    (%rbx,%rdx,8), %rdx\n\t"
        "movzx  0x06(%rdx), %r10d\n\t"
        "movzx  0x04(%rdx), %r9d\n\t"
        "and    $0x0f, %r9d\n\t"
        "mov    0x08(%rdx), %r11\n\t"
        "mov    %r11, %r9\n\t"
        "add    (%rax), %r9\n\t"
        "add    $0x04, %r8\n\t"
        "add    $0x01, %ecx\n\t"
        "add    $0x04, %rdi\n\t"
        "mov    (%rdi), %ecx\n\t"
        "mov    0x00000308(%rax), %r8\n\t"
        "mov    %ecx, %edx\n\t"
        "lea    (%r8,%rdx,4), %r8\n\t"
        "lea    (%rdx,%rdx,2), %rdx\n\t"
        "lea    (%rbx,%rdx,8), %rdx\n\t"
        "movzx  0x06(%rdx), %r10d\n\t"
        "movzx  0x04(%rdx), %r9d\n\t"
        "and    $0x0f, %r9d\n\t"
        "mov    0x08(%rdx), %r11\n\t"
        "mov    %r11, %r9\n\t"
        "add    (%rax), %r9\n\t"
        "add    $0x04, %r8\n\t"
        "add    $0x01, %ecx\n\t"
        "mov    %ecx, %edx\n\t"

        "nopl 0x22222222\n\t"
    );
    return 0;
}
