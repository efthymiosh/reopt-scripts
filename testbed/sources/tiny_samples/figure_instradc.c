#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

        "movq %rax, %rbx\n\t"
        "movq $0, %rdx\n\t"
        "addq %rcx, %rbx\n\t"
        "adcq $256, %rdx\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
