#include <stdio.h>
#define SZ 16

int main(void){
    asm volatile(
            "nopl 0x11111111\n\t"
            );

	int x[SZ][SZ], i , j;
	x[0][0] = 0;
	for (i = 0; i < SZ; i++){
		x[i][0] = i * 5;
		for(j = 1; j < SZ; j++){
			x[i][j] = x[i][j-1] * 2;
		}
	}
	for (i = 0; i < SZ; i++){
		for(j = 0; j < SZ; j++){
			printf("%5d", x[i][j]);
		}
		putchar('\n');
	}

    asm volatile(
            "nopl 0x22222222\n\t"
            );
	return 0;
}
