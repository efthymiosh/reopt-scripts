#include <stdio.h>

int main(void) {
	asm volatile (
            "nopl 0x11111111\n\t"
	);
	
		int i, x = 0;
		for (i = 0; i < 10; i++)
			x += i;
//		printf("X is: %d\n", x);

	asm volatile (
            "nopl 0x22222222\n\t"
	);
	return 0;
}
