#include <stdio.h>

int fn(int a) {
    return 0;
}

int main(void) {

	asm volatile (
            "nopl 0x11111111\n\t"

            "movq $0, %rbx\n\t"
            "movq %rsp, %rax\n\t"
            "subq $8, %rsp\n\t"
            "addq $8, %rsp\n\t"
            "movq $111, (%rsp)\n\t"
            "movq $222, (%rax)\n\t"

            "nopl 0x22222222\n\t"
            );

	return 0;
}
