#include <stdio.h>

int main(void) {
	asm volatile (
        "nopl 0x11111111\n\t"

        "movq %rax, %rbx\n\t"
        "addq %rcx, %rbx\n\t"
        "addq $256, %rcx\n\t"

        "nopl 0x22222222\n\t"
	);
	return 0;
}
