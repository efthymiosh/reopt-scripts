#include <stdio.h>

int fn(int a) {
    return 0;
}

int main(void) {

	asm volatile ("nopl 0x11111111\n\t");
    int x = 6;
    fn(4);
    x = 3;
    asm volatile ("nopl 0x22222222\n\t");

	return 0;
}
