#include <stdio.h>

int main(void) {
	asm volatile (
            "nopl 0x11111111\n\t"
            "movq $0, %rax\n\t"
            "addq $8, %rax\n\t"
            "movq $0, %rbx\n\t"
            "movq $0, %rcx\n\t"
            "nopl 0x22222222\n\t"
	);
	return 0;
}
