#include <stdio.h>

int fn(int a) {
    return 0;
}

int main(void) {

	asm volatile (
            "nopl 0x11111111\n\t"

            "movq $1, (%rsp)\n\t"
            "subq $8, %rsp\n\t"
            "movq $11, (%rsp)\n\t"
            "addq $8, %rsp\n\t"
            "movq $111, (%rsp)\n\t"

            "nopl 0x22222222\n\t"
            );

	return 0;
}
