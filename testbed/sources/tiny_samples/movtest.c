#include <stdio.h>

int main(void) {
	
	void *a = (void *)0x0;
	void *d = (void *)0x1;
	void *c[] = {(void *)0x2, (void *)0x3, (void *)0x4};
	void *b[] = {(void *)0x5, &c};
	
	__asm__ volatile (
            "nopl 0x11111111\n\t"
// 			"leaq (%0), %%rax\n\t"
// 			"leaq (%1), %%rbx\n\t"
			"leaq -0x40(%%rbp), %%rax\n\t"
			"leaq -0x30(%%rbp), %%rbx\n\t"
		
		
			"movq $5, (%%rax)\n\t"
//			"movq $8, (%%rsi)\n\t"
			"movq 8(%%rbx), %%rcx\n\t"
 			"movq 16(%%rcx), %%rdx\n\t"
 			"movq %%rdx, 8(%%rcx)\n\t"
			
            "nopl 0x22222222\n\t"
		: 
// 		: "r" (a), "r" (b)
	);
	
	return 0;
}
