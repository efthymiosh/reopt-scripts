#include <stdio.h>

int main(void) {
    int i, j, x = 0, y = 0;
    for (i = 0; i < 7; ++i) {
        if (i % 2)
            x += 3;
        else
            x += 2;
    }

    for (j = 0; j < 17; ++j) {
        if (j % 4)
            y += 3;
        else
            y += 2;
    }
    return 0;
}
