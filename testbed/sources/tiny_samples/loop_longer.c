#include <stdio.h>

int main(void){
    long long int i, j = 0, k = 0, h = 0, ar[500];
    asm volatile ("nopl 0x11111111\n\t");
    for (i = 0; i < 500; i++){
        h = k + 2;
        j = k * 2;
        k = h + j;
        ar[i] = h * k + k / 1023;
    }
    asm volatile ("nopl 0x22222222\n\t");
    return 0;
}
