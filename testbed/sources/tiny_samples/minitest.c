#include <stdio.h>

int main(void) {
	asm volatile (
            "nopl 0x11111111\n\t"
            "addq 8(%rsp), %rax\n\t"
            "nopl 0x22222222\n\t"
	);
	return 0;
}
